Capítulo 12 - Ejercicio 72
--------------------------

Un tanque cilíndrico vertical cerrado y elevado con un diámetro de $2.00 \ m$ contiene agua a una profundidad de $0.800 \ m$. Un trabajador accidentalmente hace un orificio circular de diámetro $0.02 \ m$ en la parte inferior del tanque. A medida que el agua sale del tanque mantiene una presión manométrica de $5.00 \times 10^3 \ Pa$ en la superficie del agua. Ignore los efectos de la viscosidad.

![](12.72.png)

- Inmediatamente después de que se hace el agujero, ¿cuál es la rapidez con que el agua sale del agujero?

- ¿Cuál es la razón entre esta rapidez y la rapidez del flujo de salida, si la parte superior del tanque está abierta al aire libre?


### Rapidez del chorro

$P_1 + \frac{1}{2} \rho v_1^2 + \rho g Y_1 = P_2 + \frac{1}{2}\rho v_2^2 + \rho g Y_2$

$P_1 + \frac{1}{2} \rho v_1^2 + \rho g Y_1 = \frac{1}{2}\rho v_2^2$

> $v_1 = \frac{A_2}{A_3}v_2$

$P_1 + \frac{1}{2} \rho v_2^2\left(\frac{A_2}{A_1}\right) + \rho g Y_1 = \frac{1}{2}\rho v_2^2$

$2\frac{P_1}{\rho} + v_2^2\left(\frac{A_2}{A_1}\right) + 2gY_1 = v_2^2$

$v_2^2 - v_2^2\left(\frac{A_2}{A_1}\right) = 2\frac{P_1}{\rho} - 2gY_1$

$v_2^2 \left(1 - \frac{A_2}{A_1}\right) = 2\frac{P_1}{\rho} - 2gY_1$

$v_2 = \sqrt \frac{ 2\frac{P_1}{\rho} - 2gY_1 }{ 1 - \left( \frac{A_2}{A_1} \right)^2 }$

$v_2 = \sqrt \frac{ 2\frac{5000}{1000} - 2(9.8)(0.8) }{ 1 - \left( \frac{0.00025}{6.28} \right)^2 }$

$v_2 = 5.06 \ m/s$


### Rapidez con presión atmosférica normal

$P_1 + \frac{1}{2} \rho v_1^2 + \rho g Y_1 = P_2 + \frac{1}{2}\rho v_2^2 + \rho g Y_2$

$\rho v_1^2 + 2\rho g Y_1 = \rho v_2^2$

$v_1^2 + 2g Y_1 = v_2^2$

> $v_1 = \frac{A_2}{A_3}v_2^2$

$\left(\frac{A_2}{A_3}\right)^2 v_2^2 + 2g Y_1 = v_2^2$

$v_2^2 - \left(\frac{A_2}{A_3}\right)^2 v_2^2 = 2g Y_1$

$v_2^2 \left(1 - \frac{A_2}{A_3}\right)^2 = 2g Y_1$

$v_2^2 = \frac{ 2g Y_1 }{ \left(1 - \frac{A_2}{A_3}\right)^2 }$

$v_2^2 = 2gY$

$v_2^2 = 15.68$

$v_2 = 3.95 \ m/s$

### Razón de velodicidades

$razon = \frac{5.06}{3.95}$

$razon = 1.28$

- - - 

**Respuestas:**

- La velocidad de salida es de 5.06 m/s

- La razón de la velocidad de salida con el recipiente sellado contra el recipiente abierto es de 1.28