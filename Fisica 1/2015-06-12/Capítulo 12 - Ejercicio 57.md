Capítulo 12 - Ejercicio 57
--------------------------

Un cubo de 0.180 kg de hielo (agua congelada) está flotando en glicerina. La glicerina se encuentra en un cilindro alto que tiene un radio interno de 3.5 cm. El nivel de la glicerina está muy por debado de la parte superior del cilindro. Si el hielo se derrite por completo. 

¿En qué distancia cambia la altura del hielo en el cilindro?
¿La superficie del agua está arriba o debajo del nivel original de glicerina antes de que se derrita el hielo?

![](12.57.png)

Hielo:

$m = 0.18 \ kg = 180 \ g$

$\rho_{hielo} = 920 \ kg / m^3  = 0.92 \ g/cm^3$

Glicerina:

$\rho = 1260 \ kg/m^3 = 1.26 \ g/cm^2$

Recipiente:

$r = 3.5 \ cm$


### Porcentaje sumergido

$Porcentaje = \frac{\rho _{hielo}}{\rho _{glicerina}} 100\% = 73\%$

### Volumen de glicerina desplazada

$\rho = \frac{m}{v}$

$v = \frac{m}{\rho}$

$v = \frac{180 \times 73\%}{1.26}$

$v = 82.125 \ cm^3$

### Altura elevada

$v = \pi r^2 h$

$h = \frac{v}{\pi r^2}$

$h = \frac{82.125}{3.14(3.5^2)}$

$h = 2.13 \ cm$

### Volumen del hielo al derretirse

$\rho = \frac{m}{v}$

$v = \frac{m}{\rho_{agua}}$

$v = 180 \ cm^2$

### Altura de ese volumen en el recipiente

$v = \pi r^2 h$

$h = \frac{v}{\pi r^2}$

$h = \frac{180}{3.14 (3.5^2)}$

- - -

**Respuesta:**

- La altura se eleva $2.13 \ cm$

- Hay más líquido estando el hielo derretido.




