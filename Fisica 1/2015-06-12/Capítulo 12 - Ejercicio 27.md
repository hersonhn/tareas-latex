Capítulo 12 - Ejercicio 27
--------------------------

Una masa de mineral pesa 17.5 N en el aire, pero se cuelga de un hilo ligero y se sumerge por completo en agua, la tensión en el hilo es de 11.2 N. Calcule el volumen total y la densidad de la muestra.

![](12.27.png)

$W = 17.5 \ N$  
$W = Mg$  
$17.5 = M(9.8)$  
$M = \frac{17.5}{9.8} = 1.785 \ kg$

$T_{sumergido}=11.20 \ N$

- - -

$Peso Aprarente = Mg - B$  
$11.20 = 17.50 - B$  
$B = 17.5 - 11.2$  
$B = 6.3$

- - -

$B = \rho_{agua} V_D g$

$V_D = \frac{B}{\rho_{agua} g}$

$V_D = \frac{6.3}{9800}$

$V_D = 6.428 \times 10^{-4} \ m^3$

- - -


$\rho = \frac{B}{V}$

$\rho = \frac{1.7285}{6.428 \times 10^{-4}}$

$\rho = 2776.91 kg/m^3$

- - -

**Respuesta:**

El volumen total es de $6.428 \times 10^{-4} \ m^3$, mientras que la densidad es de $2,776.91 \ kg/m^3$

