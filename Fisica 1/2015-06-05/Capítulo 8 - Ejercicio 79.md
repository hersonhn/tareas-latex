Sección 8: Ejercicio 79
-----------------------

Un trozo de hielo de 5 kg se desliza a 12 m/s sobre el piso de un valle cubierto de hielo cuando choca y se adiere con otro pedazo de hielo de 5 kg que estaba en reposo. Como el valle tiene hielo, no hay fricción. Después del choque, ¿qué altura sobre el suelo del valle alcanzan los pedazos combinados?

![](08.79.png)

$v_1 = 12 \ m/s$  

$m_1 = 5 kg$

$v_2 = 0 \ m/s$

$m_2 = 5 kg$


### Velocidad de los bloques combinados


$v_f = \frac{m_1 v_ {i2} + m_2 v_{i2}}{m_1 + m_2}$
 
$v_f = \frac{5 (12) + 5 (0)}{5 + 5}$

$v_f = 6 \ m/s$


### Altura alcanzada al llegar a la velocidad 0.

$w_{neto} = \Delta K$

$-MgH = \frac{1}{2} M(v_f^2 - v_i^2)$

$gH = \frac{1}{2}v_1^2$

$H = \frac{1}{2}6^2$

$H = 18 \ m$

**Respuesta:**

La altura alcanzada por ambos bloques de hielo juntos, es de 18 metros.






