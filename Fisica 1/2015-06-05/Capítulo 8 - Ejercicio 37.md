Sección 8: Ejercicio 37
-----------------------

En un campo de fútbol muy lodoso, un apoyador de 110 kg taclea a un corredor de 85 kg. Justo antes del choque el apoyador resbala con una velocidad de 8.8 m/s hacia el norte, y el corredor con una velocidad de 7.2 m/s hacia el este. ¿Con qué velocidad (magnitud y dirección) se mueven juntos los dos jugadores inmediatamente después del choque?

![](08.37.png)

$m_1 = 85 \ kg$  
$v_1 = 7.2 \ m/s$

$m_2 = 110 \ kg$  
$v_2 = 8.8 \ m/s$

$\vec{P_I} = \vec{P_F}$

$m_1 v_1 \hat i + m_2 v_2 \hat j = (m_1 + m_2) v_f$  
$m_1 v_1 \hat i + m_2 v_2 \hat j = ((m_1 + m_2) v_f \cos \theta) \hat i + ((m_1 + m_2) v_f \sin \theta) \hat j$  
$m_1 v_1 \hat i + m_2 v_2 \hat j = ((m_1 + m_2) v_f \cos \theta) \hat i + ((m_1 + m_2) v_f \sin \theta) \hat j$


$m_1 v_1 = (m_1 + m_2) v_f \cos \theta$  
$m_2 v_2 = (m_1 + m_2) v_f \sin \theta$

$612 = (195) v_f \cos \theta$  
$968 = (195) v_f \sin \theta$

$v_f = \frac{612}{(195) \cos \theta}$  
$v_f = \frac{968}{(195) \sin \theta}$


$\frac{612}{(195) \cos \theta} = \frac{968}{(195) \sin \theta}$

$968 \cos \theta = 612 \sin \theta$

$\frac{968}{612} = \frac{\sin \theta}{\cos \theta}$

$\tan \theta = \frac{968}{612}$

$\theta = \arctan \frac{968}{612}$

$\theta = 57.7º$

---

$v_f = \frac{612}{197 \cos 57.7}$

$v_f = 5.8 \ m/s$

**Respuesta:**

La velocidad final es de 5.8 m/s.

