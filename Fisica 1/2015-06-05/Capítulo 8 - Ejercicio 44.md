Sección 8: Ejercicio 44
-----------------------

Un bloque de 10 kg está sujeto a un resorte horizontal muy ligero con constante fuerza de 500 N/m, que reposa sobre una mesa horizontal sin fricción. De repente, es golpeado por una piedra de 3 kg que viaja de forma horizontal a 8 m/s hacia la derecha, con lo cual la piedra rebota horizontalmente a 2 m/s hacia la izquierda. Calcule la distancia máxima que el bloque comprime el resorte después del choque.

![](08.44.png)


### Al golpear el resorte.

$v_f = \frac{(m_2 - e m_1)v_{i_2} + m_1(1 + e)v_1}{m_1 + m_2}$

$v_f = \frac{(15 - (1)3)(0) + (3)(1 + 1)(8)}{3 + 15}$

$v_f = 2.66$

### Resorte

$W_{neto} = \Delta K$

$\frac{1}{2} k d = \frac{1}{2} m(v_f^2 - v_i^2)$

$d = \frac{0.5 m(v_f^2)}{0.5 k}$

$d = \frac{15(2.66^2)}{500}$

$d = 0.21$

**Respuesta:**

El resorte se comprime 21 centímetros.