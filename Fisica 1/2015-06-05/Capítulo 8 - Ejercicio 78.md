Sección 8: Ejercicio 78
-----------------------

Un pequeño bloque de madera de 0.8 kg de masa está suspendido del extremo inferior de una cuerda ligera de 1.6 m de logintud. El bloque está en reposo inicialmente. Una bala de 12 g de masa se dispara al bloque con una velocidad horizontal de $v_0$. La bala golpea el bloque y se incrusta en él. Después del choque, el objeto combinado oscila en el extremo de la cuerda. Cuando el bloque se eleba una altura vertical de 0.8 m, la tensión en la cuerda es de 4.8 N ¿Cuál es la velocidad inicial $v_0$ de la bala?

![](08.78.png)

$v = \left(1 + \frac{M}{m}\right)\sqrt{2gh}$  
$v = \left(1 + \frac{800}{12}\right)\sqrt{2(9.8)(0.8)}$  
$v = 267 m/s$

**Respuesta:**

La velocidad inicial de la bala es de 267 m/s.