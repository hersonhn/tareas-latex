Ejercicio 2
===========

Okoman el super hombre corre en una pista de 100 m, parte del reposo, moviendose a aceleracion constante recorre los primeros 10 m en 5 seg. ¿En cuanto tiempo recorre los ultimos 35 m?

| Punto Inicial | Punto Final   | Distancia |
|:-------------:|:-------------:| ---------:|
| A             | B             |      10 m |
| A             | D             |     100 m |
| C             | D             |      35 m |


### Buscar la aceleración en $a \to b$  

$t = 5s$

$d = 10m$

$a = ?$

$d = \frac{1}{2}at^2$

$a = \frac{2d}{t^2}$

$a = \frac{(2)(10)}{5^2}$

### Buscar tiempo en $a \to c$ 

$d = \frac{1}{2}at^2$

$t = \sqrt{\frac{d}{\frac{1}{2}a}} = \sqrt{\frac{2.65}{0.8}}$ 


### Buscar velocidad final en $a \to c$

$a = 0.8 m/s$

$d = 65m$

$v_0 = 0$

$v_f = at$

$v_f = (0.8) (12.74)$

$v_f = 10.192 m/s$

### Buscar el tiempo $a \to d$

$v_i = 0$

$a = 0.8 m/s$

$d = 100 m$

$d = \frac{1}{2}a - t^2$

$t^2 = \frac{d}{\frac{1}{2}a}$

$t = \sqrt{\frac{2d}{a}}$

$t = \sqrt{\frac{200}{0.8}}$

$t = 15.81$


### Buscar la velocidad final $a \to d$

$v_f = at$

$v_f = (0.8) (15.81)$

$v_f = 12.64$

### Buscar Tiempo $c \to d$

$v_i 10.192$

$v_f = 12.64$

$a = 0.8$

$t = \frac{v_f - v_i}{a} $

$t = \frac{12.64 - 10.19}{0.8}$

$t = 3.06$

**Respuesta:**

Se tarda un total de 3.06 segundos.