Ejercicio 5
===========

Se patea un balon hacia arriba a una velocidad de 30 m/seg, 2 segundos despues se lanza otro balon.

¿Que velocidad inicial debe tener el segundo balon para alcanzar al primero a 36 m del suelo?


### Balón 1

Velocidad a los 2 seg.

$v_f = v_i + at$

$v_f = 30 + (-9.8)(2)$

$v_f = 10.4$

Posición final en Y a los 2 seg.

$y_f = v_i t + \frac{1}{2}at^2$

$y_f = (30)(2) + \frac{1}{2} (-9.8)(2)^2$


Estas distancia y posición finales serán las iniciales al monento de lanzar el segundo balón.

- - -

### Balón 1

Tiempo al alcanzar los 36 m

$t =\ ?$

$y_f = y_i + v_i t + \frac{1}{2} a t^2$

$36 = 0 + 30t + \frac{1}{2} (-9.8) t^2$

$36 = 30t - 4.9t^2$

$4.9 t^2 - 30 t + 36 = 0$

$t = \frac{-b ± \sqrt{b^2 - 4ac}}{2a}$

$t = \frac{30 ± \sqrt{30^2 - (4)(36)(9.4)}}{(2)(4.9)}$

$t = \frac{30 ± \sqrt{900 - 705.6}}{9.8}$

$t = \frac{30}{9.8} ± \frac{13.92}{9.8}$

$t_1 = 4.48 \ \ \ \  t_2 = 1.6$

Los balones se enuentran a los 1.6 y 4.48 segundos (al subir y al bajar respectivamente), tomamos 1.6 como el tiempo que usaremos.

- - - 

### Balon 2

$v_i = \ ?$

$y_i = 0 m$

$y_f = 40.4 m$

$a = -9.8 m/s^2$

$t = 1.6 s$

$y_f = y_i + v_i t + \frac{1}{2} a t^2$

$40.4 = 0 + v_i (1.6) + \frac{1}{2} (-9.8)(1.6)^2$

$v_i (1.6) = 40.4 - \frac{1}{2} (-9.8)(1.6)^2$

$v_i = \frac{40.4}{1.6} - \frac{1}{2} (-9.8)(1.6)$

$v_i = 17.41$

**Respuesta:**

La velocidad a la que debe ser lanzado el segundo balón es de 17.41 m/s










