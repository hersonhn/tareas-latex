Ejercicio 3
===========

Serapio viajando en su auto deportivo a velocidad constante de 40 m/s pasa a un polica que esta estacionado en su motocicleta a la orilla de la calle. El polica observa que Serapio va a exceso de velocidad por lo que decide perseguirlo. El polica arranca un segundo despues de que Serapio paso junto a el. Si la aceleracion del polica es 4 m/s²

¿Cuanto recorre el policía antes de alcanzar a Serapio?

**Moto:**
$v = 40 m/s$
**Auto:**
$a = 4 m/s^2$


**[Moto]** $x = \frac{1}{2}at^2$
**[Auto]** $x = vt$



$\frac{1}{2}a_m t_m^2 = v_a t_a$

$\frac{1}{2}a_m(t_a - 1)^2 = v_a t_a$

$t_a^2 - 2t_a + 1 = \frac{2v_a}{a_m} t_a$

$t_a^2 - (2 + \frac{(2)(40)}{4})t_a + 1 = 0$

$t_a^2 - 22 t_a + 1 = 0$

$t_a = \frac{-b ± \sqrt{b^2 - 4ac}}{2a}$

$t_a = \frac{22 ± \sqrt{22 - 4}}{4}$

$t_a = \frac{22}{4} ± \sqrt\frac{386}{4}$

$t_a = 15.32$

Distancia recorrida por el auto:

$x = 40(15.32)$

$vt = 40(15.32)$

**Respuesta:**

El auto recorrió 612 metros al igual que a moto cuando esta lo alcanzó.




