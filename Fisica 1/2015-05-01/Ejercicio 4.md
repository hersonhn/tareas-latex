Ejercicio 4
===========

Okoman el super heroe observa a Prosilapia a 40 m abajo con una rapidez de 5 m/s hacia abajo, por lo que se dirije hacia ella partiendo del reposo con aceleracion constante. Si luego de 4 segundos esta a 35 m de ella, determinar el tiempo en que Okoman alcanza a Prosilapia. 

### Okoman

$a =\ ?$

$y_f = y_i + v_it + \frac{1}{2}at^2$

$-5 = \frac{1}{2}(a)(4)^2$

$-5 = 8a$

$a = -\frac{5}{8}$

**Okoman:** $y_f = y_i + v_i \frac{1}{2}at^2 = \frac{1}{2}\frac{-5}{8}t^2$

**Prosilapia:** $y_f = y_i + v_it + \frac{1}{2}(-9.8)t^2$

$\frac{1}{2}\frac{-5}{8}t^2 = \frac{1}{2}(-9.8)t^2$

$\left( \frac{1}{2}\frac{-5}{8}t^2 = \frac{1}{2}(-9.8)t^2 \right) (2)$

$\left( \frac{-5}{8}t^2 = (-9.8)t^2 \right) \left(-\frac{1}{t^2}\right)$

$(\frac{5}{8} - 9.8) t = 80$

$t = \frac{80}{\frac{5}{8} - 9.8}$

**Respuesta:** 

El tiempo es negativo, y como la aceleración de Prosilapia es mayor que la de Okoman, se puede deducir que este nunca la alcanza.