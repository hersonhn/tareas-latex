Ejercicio 1
===========

Nando y Marco compiten en una carrera de 100 m, ambos parten del reposo. Nando muy confiado arranca 5 segundos despues que Marco. La aceleracion constante de Marco es 3 m/s². Si ambos llegan a la meta al mismo tiempo determinar la aceleracion constante de Nando. 

$N_d = M_d = 100 m$

$M_a = 3 m/s^2$

$N_t = M_t - 5$

### Tiempo de Marco

$x = V_0 t + \frac{1}{2}at^2$

$100 = \frac{3}{2}t^2$

$t = \sqrt{\frac{200}{3}}$

$t = 8.1s$

### Tiempo de Nando

$N_t = M_t - 5$

$N_t = 8.1 - 5$

$N_t = 3.1s$

### Aceleración de Nando

$d = V_o t + \frac{1}{2}at^2$

$100 = \frac{1}{2}a(3.1)^2$

$a = \frac{200}{3.1^2}$

$a = 20 m/s^2$

**Respuesta:**

La aceleración de Nando es de  $20 m/s^2$