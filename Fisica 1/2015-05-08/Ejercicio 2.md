## Ejercicio 2

Nando desde el suelo lanza una piedra con cierta inclinación, si luego de 2 segundos la altura de la piedra es 2m; determinar el tiempo de vuelo (tiempo desde que se lanzó hasta que regresa al suelo).


$y = v_0 y + \frac{1}{2}at^2$

$2 = v_0 (2) + \frac{1}{2}(-9.8)2^2$

$1 = v_0 - 9.8$

$v_0 = 10.8 \ m/s$

Tiempo en alcanzar la altura máxima/:

$v_f = v_0 + at$
$0 = 10.8 - 9.8 t$

$-10.8 = -9.8t$

$t = \frac{10.8}{9.8}$

$t = 1.1$

Tiempo total del recorrido:

$T = 2t$

$T = 2(1.1)$

$T = 2.2 \ m$

**Respuesta:**

El tiempo total del vuelo es de 2.2 metros.π