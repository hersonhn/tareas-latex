## Ejercicio 4

Una piedra es arrojada sobre un acantilado que tiene una altura h con una rapidez inicial de 42 m/s a 60º sobre la horizontal.  La piedra se estrella en el punto A 5.5 seg despues delanzada.

1. Encontrar la altura del acantilado.
2. La máxima altura que alcanza la piedra.


Componentes en X y Y:

$v_{_{0}x} = v_0 \cos \alpha_0 = 42 \cos 60 = 21 \ m/s$
$v_{_{0}y} = v_0 \sin \alpha_0 = 42 \sin 60 = 36.37 \ m/s$

Tiempo en llegar al punto más alto:

$v_f = v_0 + at$

$\frac{v_f - v_0}{a} = t$

$t = \frac{0 - 42}{-9.8}$

$t = 4.28 \ s$

Si el tiempo en llegar al punto final es mayor que el tiempo de choque, podemos afirmar que pasó por el punto más alto.

- - -

Tiempo en volver a la misma altura desde la que se lanzó:

$T = 2t$

$T = 2t$

$T = 8.56 \ s$

Alcance horizontal de la piedra:

$x = v_0 \cos \theta t$

$x = (42)(\cos 60)(5.5)$

$x = 115.5 \ m$

Altura máxima:

$y_{max} = \frac{v_0^2 \sin 2\theta}{2g}$

$y_{max} = \frac{42^2 \sin 120}{2(9.8)}$

$y_{max} = 77.98 \ m$

Posición vertical de la piedra a en $t = 5.5$:

$y(t) = v_0  t \sin \theta - g\frac{t^2}{2}$

$y(5.5) = (42)  (5.5) \sin 60 - (9.8) \frac{(5.5)^2}{2}$

$y = 51.82$

Altura del acantilado:

$h = v_{max} - y(t)$

$h = 77.98 - 51.82$

$h = 26.16 \ m$


**Respuestas:**

La altura máxima que alcanza la piedra es de 77.98 metros.

La altura del acantilado es 26.16 metros.