## Ejercicio 3

Se lanza desde el suelo un celular con una rapidez de 30 m/s con una dirección de 60º sobre la horizontal. 

Determinar la rapidez del proyectil luego de 4 segundos.

$v_o = 30 \ m/s$  
$\theta = 60º$

Componentes en X y Y:

$v_{_{0}x} = v_0 \cos \alpha_0 = 30 \cos 60 = 13 \ m/s$
$v_{_{0}y} = v_0 \sin \alpha_0 = 30 \sin 60 = 25.98 \ m/s$

Velocidad en Y cuando $t = 0$:

$v_f = v_o + at$

$v_f = 25.98 + (-9.8) 4$

$v_f = -13.22 \ m/s$

Velocidad del proyectil:

$v = \sqrt{v_x^2 + v_y^2}$

$v = \sqrt{15^2 + (-13.22)^2}$

$v = 19.99 \ m/s$

**Respuesta:**

La rapidez del proyectil a los 4 segundos, es de 19.99 m/s