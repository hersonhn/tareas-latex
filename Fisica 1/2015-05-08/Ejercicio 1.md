## Ejercicio 1

Un proyectil es lanzado con una velocidad 30 m/s de manera que forma 40º con la horizontal. 

Calcular la velocidad del proyectil en su punto mas alto.

$v_o = 30 \ m/s$  
$\theta = 40º$

Componentes en X y Y:

$v_{_{0}x} = v_0 \cos \alpha_0 = 30 \cos 40 = 21.98 m/s$
$v_{_{0}y} = v_0 \sin \alpha_0 = 30 \sin 40 = 19.28 m/s$

$v_y = v_{_{0}y} - gt = 0$  

$0 = v_{_{0}y} - gt$  

$gt = v_{_{0}y}$  

$t = \frac{v_{_{0}y}}{g}$  

$t = \frac{19.28}{9.8}$

$t = 1.967 \ seg$

Velocidad en el punto máximo:

$v = \sqrt{v_y^2 + v_x^2} = \sqrt{v_x^2}$

$v = 22.98 \ m/s$ 

**Respuesta:**

La velocidad en el punto máximos es de 22.98 m/s