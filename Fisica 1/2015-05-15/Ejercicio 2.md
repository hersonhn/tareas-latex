Ejercicio 2
===

Se mueve un bloque de masa 80 kg mediante una fuerza P. El coeficiente de fricción cinética entre el bloque y la superficie es de 0.7. Los ángulos indicados en la figura son $θ = 25º$ y $θ = 35º$. Determinar el valor de la fuerza para que el bloque se dirija hacia arriba (en dirección del plano inclinado) a velocidad constante.

