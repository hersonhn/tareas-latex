Ejercicio 1
===

Un bloque de 3 kg parte del reposo en la parte superior de una pendiente de 30∘ y se desliza 2 m hacia abajo en 15 seg. Encontrar  
                             

a) La magnitud de la aceleración del bloque

b) El coeficiente de fricción cinética entre el bloque y el plano

c) La fuerza de fricción que actúa sobre el bloque.

d) La rapidez del bloque luego que se ha deslizado 2 m.

### magnitud de la aceleración

$a = g \sin 30$

$a = \frac{1}{2} 9.8$

$a = 4.9 \ m/s^2$

### coeficiente de fricción

$\mu = \frac{F_f}{F_n}$

$F_f = 3 (9.8) \sin \theta = 14.7$

$F_n = 3 (9.8) \cos \theta = 25.48$

$\mu = 0.577$

### fuerza de fricción que actúa sobre el bloque

$F = mg \sin \theta$

$F = 3(9.8) \sin 30$

$F = 14.7 \ m/s^2$