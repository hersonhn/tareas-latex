Ejercicio 4
===

Dos bloques de igual masa se conectan mediante una cuerda de masa despreciable a través de una polea sin fricción según se muestra en la figura. Si $θ=30º$ y $μk=0.27$ 

¿Cuál es el valor de la aceleración de los bloques?


$\sum F x = M_1 a$

$-fk - mg \sin \theta + T = M_1 a$

$\sum f_y = 0$

$n - mg \cos \theta = 0$
$n = m_1g \cos \theta = 0$
$n = 8.48 \ N$

$Fk = Mkn$

$Fk =0.27 (8.48)$

$Fk =2.28$

$a = \frac{-fk -mg \sin \theta + mg}{m_1 + m_1}$

$a = 1.31 m/s^2$

**Respuesta**

La aceleración es de $1.31 m/s^2$