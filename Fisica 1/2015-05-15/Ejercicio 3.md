Ejercicio 3
===

En la figura una fuerza F de magnitud 12 N es aplicada a una caja de masa $m_2 = 1 \ kg$. La fuerza tiene la dirección del plano inclinado a 37º. La caja está conectada por una cuerda a otra caja de masa $m_1 = 3 \ kg$ que está en el suelo. Todas las superficies son lisas y la polea no presenta fricción. ¿Cuál es la tensión de la cuerda? 

$\sum F_x = MA$

$(F - T - Mg \sin \theta = M_2 A) -$
$( T - M_1 g \sin \theta = M_2 A) =$

$F - M_2 g \sin \theta - M_1 g \sin \theta = A (M_2 + M_1)$


$\frac{F - M_2 g \sin \theta - M_1 g \sin \theta }{M_2 + M_1} = A$

$\frac{12 - (1)(9.8) (0.6018) - 3 (9.8) 0 }{1 + 3} = A$

$A = 1.525 \ m/s^2$

$T - Mg \sin \theta = M_1 A$

$T = M_1 A + M_1 g \sin \theta$

$T = 3(1.525) + 3 (9.8) (0)$

$T = 4.575 \ N$

**Respuesta**

La tensión es de 4.575 Newtons


