## Ejercicio 1

Una caja cúbica, de 5.1 x 10<sup>-2</sup> m<sup>3</sup> de volumen, está llena con aire a presión atmosférica y 20ºC. La caja está cerrada y se calienta a 180ºC. ¿Cuál es la fuerza neta sobre cada lado de la caja?


$v = 51x10^{-2} \ m^3$  
$l = 0.37$   
$p_1 = 1 atm$  
$T_1 = 20ºC = 293.15 K$  

$PV = RnT$

$\frac{P_1}{V_1} = \frac{Rn}{V}$

$\frac{P_2}{V_2} = \frac{Rn}{V}$

$\frac{P_1}{V_1} = \frac{P_2}{V_2}$

$\frac{1}{293.15} = \frac{P_2}{453.15}$

$453.15 = 293.15 P_2$

$P_2 = \frac{453.15}{293.15}$

$P_2 = 1.5 atm$


Presión por cada cara:

$1.5 atm = 1.52x10^6 \ N/m^2$ 

$P_{cara} = \frac{1.52x10^6}{6}$ 

$P_{cara} = 253,333.33 \ N/m^2$

### Respuesta:

La presión es de 253,333.33 Newtons por metro cuadrado en cada cara.
