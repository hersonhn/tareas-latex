
### Ejercicio 3

Estime cuántas moléculas de aire hay en cada inhalación de 2 L que realiza una persona, y que también estuvieron en el último aliento de Galileo.

Suponga que la atmósfera tiene aproximadamente 10 km de alto y densidad constante.

$T = 20ºC = 293.15 K$

##### Densidad

$P_h = P_0e^{-mgh /TK}$

$P = (1.0)e^{\frac{-(0.028)(9.8)(10000)}{(293.15)(8.314)}}$

$P = 0.32 atm$

### En una inhalación de 2 litros. 

$PV = nRT$

número de moles

$n = \frac{PV}{RT}$

$n = \frac{(0.32)(2)}{(293.15)(0.082)}$

$n = 0.026$

número de moléculas

$N_m = N_A n$

$N_A = 6.022×10^{23}$

$N_m = (6.022×10^{23})(0.026)$

$N_m = 1.56x10^{22}$

- - -

### En el "último aliento" de Galileo

- **Volumen corriente (V<sub>C</sub>):** Volumen de una exhalación normal.
- **Volumen de reserva espiratorio (V<sub>RE</sub>):** Cantidad de aire adicional en los pulmones (0.5 L).
- **Ultimo Aliento:** Volumen corriente + Volumen de reserva espiratorio (1.1 L)

$V_C = 0.5 L$  
$V_{RE} = 1.1 L$  
$V_{UA} = V_C + V_{RE} = 1.6$

$PV_{UA} = nRT$

número de moles

$n = \frac{PV_{UA}}{RT}$

$n = \frac{(0.32)(1.6)}{(293.15)(0.082)}$

$n = 0.021$

número de moléculas

$N_m = N_A n$

$N_A = 6.022×10^{23}$

$N_m = (6.022×10^{23})(0.026)$

$N_m = 1.26x10^{22}$

### Respuestas

- **Inhalación de 2 L:** 1.56x10<sup>22</sup>
- **Último aliento de Galileo:** 1.26x10<sup>22</sup>

