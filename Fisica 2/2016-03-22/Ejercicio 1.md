## Ejercicio 1

¿Cuál es la presión dentro de un contenedor de 35 L en cuyo interior hay 105 kg de gas argón a 385 K?

$PV = RnT$

$R = 0.0821$  
$m_u = 39.948$  
$m = 105 kg$  
$v = 35 L$  
$T = 385 K$

$n = \frac{105,000}{39}$

$P = \frac{RnT}{V}$

$P = \frac{(0.082)(2628.41)(385)}{35}$

$P = 2370.8$

### Respuesta:

La presión es de 2370 atm.