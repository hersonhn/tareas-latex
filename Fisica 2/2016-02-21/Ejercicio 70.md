## Ejercicio 70

Una macetera es arrojada desde un balcón a una altura de $d = 20.0 m$ sobre la banqueta y cae sobre un hombre despreveinido de estatura $h = 1.75 m$ que está parado abajo. Suponga que el hombre requiere de $0.300 s$ para responder a la advertencia. ¿Qué tan cerca de la banqueta puede caer la macetera antes que sea demasiado tarde como para prevenir al hombre gritando desde el balcón?

$\Delta h = 20 - 1.75$  
$\Delta h = 18.25$  

#### Tiempo de la Macetera

$d = V_0 t + \frac{1}{2}at^2$  
$d = \frac{1}{2}at^2$  
$t_1 = \sqrt{2 \frac{d}{a}}$  
$t_1 = \sqrt{2 \frac{18.25}{9.8}}$  
$t_1 = 1.92$

#### Tiempo del Sonido

$v = 344 m/s$  
$v = \frac{d}{t}$  
$t_2 = \frac{d}{v}$  
$t_2 = \frac{18.25}{344}$  
$t_2 = 0.053$

$t_s = 0.300 + t_2$  
$t_s = 0.353$

#### Posición de la macetera a $t_s = 0.353$

$d = v_0 t + \frac{1}{2} a t^2$  
$d = \frac{1}{2} (9.8) (0.353)^2$  
$d = 0.61$

### Distancia de la banqueta en $t_s = 0.353$

$20 - 0.61 = 19.39$