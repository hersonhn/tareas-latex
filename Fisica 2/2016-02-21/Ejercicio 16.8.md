## Ejercicio 16.8

A 27.0 ºC ¿qué rapidez tienen las ondas longitudinales de:

- Hidrogeno (masa molar 2.02 g/mol)?
- Helio (masa molar 4.00 g/mol)?
- Argón (masa molar 39.9 g/mol)?

##### Hidrogeno

$v = \sqrt \frac{\gamma RT}{m}$

$m = 2.02$  
$R = 8.314$  
$T = 300.15$  
$\gamma = 14.414$

$v = \sqrt \frac{(14.414) (8.314) (300.15)}{2.02}$  
$v = 133.441$

$Rapidez = 133.441$

##### Helio

$v = \sqrt \frac{\gamma RT}{m}$

$m = 2.02$  
$R = 8.314$  
$T = 300.15$  
$\gamma = 20.786$

$v = \sqrt \frac{(20.786) (8.314) (300.15)}{4.00}$  
$v = 113.875$

$Rapidez = 113.875$

##### Argón

$v = \sqrt \frac{\gamma RT}{m}$

$m = 39.9$  
$R = 8.314$  
$T = 300.15$  
$\gamma = 20.786$

$v = \sqrt \frac{(20.786) (8.314) (300.15)}{39.9}$  
$v = 36.05$

$Rapidez = 36.05$



