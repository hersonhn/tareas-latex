## Ejercicio 1

Periodo: $P = 10$  
Amplitud: $A = 10$

Frecuencia:

$F = \frac{1}{P}$
$F = 0.1$

Velocidad Angular:

$\omega = 2\pi f$
$\omega = 2(3.14)(0.1)$
$\omega = 0.6283$

#### Respuesta

La velocidad angular es de 0.6283

