## Ejercicio 3

Periodo: $P = 1.2s$  
Amplitud: $A = 0.6m$  
Tiempo: $t = 0.48$  
Desplazamiento: $\phi = 0$

Frecuencia: 

$F = \frac{1}{P}$
$F = 0.83$

Velocidad angular:

$\omega = 2\pi f$
$\omega = 2(3.14)(0.83)$
$\omega = 5.2124$


Posición:

$x(t) = A \cos(\omega t + \phi)$

$x(t) = 0.6 \cos((5.2124)(0.48) + 0)$

$x = -0.48$


#### Respuesta

A los 0.48 segundos está a -0.48 unidades de distancia del punto de equilibrio.
