## Ejercicio 2

Masa original: $m_0 = 0.75kg$  
Frecuencia original: $1.33 Hz$

$\omega^2 = \frac{k}{m} = (2 \pi f)^2$

$m_1 f_1^2 = m_0f_0^2 = m_2f_2^2$

**Agregándole 0.220 kg**

$f' = F \sqrt{\frac{m_0}{m_1}}$

$f' = 1.33 \sqrt{\frac{0.75}{0.97}}$

$f' = 1.77 Hz$

**Restándole 0.220 kg**

$f' = F \sqrt{\frac{m_0}{m_2}}$

$f' = 1.33 \sqrt{\frac{0.75}{0.53}}$

$f' = 1.58 Hz$


### Respuestas:

- Agregándole 0.220 kg $\to$ 1.77 Hz
- Restándole 0.220 kg $\to$ 1.58 Hz


