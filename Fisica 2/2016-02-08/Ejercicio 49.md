## Ejercicio 49

Un gran bloque P realiza movimiento armonico simple horizontal mientras se desliza a traves de una superficie sin fricción, con una frecuencia F de 1.50 Hz. El bloque B descansa sobre él, como se muestra en al figura y el coeficiente de fricción estático entre los dos es de $\mu = 0.600$ 

¿Qué amplitud máxima de oscilación puede tener el sistema si el bloque B no se desliza?


$F = 1.50$  
$F = \frac{\omega}{2\pi}$  
$\omega = 2\pi f$  
$\omega = 2(3.14)(1.5)$  
$\omega = 9.42$

$A = a \omega^2 = \frac{f}{m}$ $(f = m_s m g)$


$A = \frac{(0.6)(9.81)}{9.42} = \frac{5.88}{28.27} = 0.208m$

### Respuesta

Oscila máximo 0.208 metros.

