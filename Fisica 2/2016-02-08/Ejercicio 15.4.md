## Ejercicio 15.4


Se llama ultrasonido a las frecuencias más arriba de la gama que puede detectar el odio humano, esto es sonidos mayores que 20,000 Hz.

$v = \lambda f$

$f = \frac{v}{\lambda} = \frac{1500}{10^{-3}}$


