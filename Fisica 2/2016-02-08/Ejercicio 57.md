## Ejercicio 57

Un tablón horizontal de masa m y longitud L se articula en un extremo. El otro extremos del tablón está sostenido por un resorte con constante de fuerza $k$.

Demuestre que el tablón se mueve con movimiento armónico simple con frecuencia angular $w = \frac{\sqrt{3k}}{m}$


$w = \frac{\sqrt k}{I}$ 

$I = \frac{1}{3mL^2}$

$k = \frac{w}{I} = \frac{\sqrt 3}{1/3}$

$f = \frac{\pi}{2}$

$\sqrt \frac{k}{I} = \frac{1}{2\pi} \sqrt\frac{3 k/m}{1/3 mL^2}$

Evalue la frecuencia, considere que la masa es de 5 kg y la constante $K$ del resorte es de 100 N/m

$\omega \sqrt\frac{K}{m} = \sqrt\frac{100 N/m}{5 kg} = 4.46$

$F = \frac{\omega}{2\pi} = \frac{4.47}{2 \pi} = 0.71$


