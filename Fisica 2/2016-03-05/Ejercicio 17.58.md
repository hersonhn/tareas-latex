## Ejercicio 17.58

Un técnico de laboratorio pone una muestra de 0.0850 kg de un material desconocido, que está a 100.0ºC, en un calorímetro cuyo recipiente, inicialmente está a 19.0ºC, hecho con 0.150 kg de cobre y contiene 0.200 kg de agua. La temperatura final del calorímetro es de 26.1ºC.  
**Calcule el calor específico de la muestra.**

T<sub>x</sub> = 100ºC = 373.15 k  
m<sub>x</sub> = 0.085 kg

T<sub>r</sub> = 19ºC = 292.15 k  
m<sub>r</sub> = 0.15 kg  
c<sub>r</sub> = 390 J/kg • K

m<sub>a</sub> = 0.2 kg  
c<sub>a</sub> = 4.19x10<sup>3</sup>

T<sub>f</sub> = 26.1ºC = 299.25 K

### Agua

$\Delta T_a = 26.1 - 19 = 7.1$

$Q_a = m_a c_a \Delta T_a$  
$Q_a = (0.2)(4.19x10^3)(7.1)$  
$Q_a = 5.45x10^3$

### Recipiente

$Q_r = m_r c_r \Delta T_r$  
$Q_r = (0.15)(390)(7.1)$  
$Q_r = 415$


### Muestra

$Q_m = m_m c_m \Delta T_m$  
$Q_m + Q_r + Q_a = 0$

$Q_m = (0.085)c_m(-73.9)$  

$-(0.085)c_m(-73.9) = 415 + 5.45x10^3$

$c_m = \frac{415 + 5.45x10^3}{(0.085)(73.9)}$

$c_m = 3.6x10^6$

### Respuesta

El calor específico de la muestra es del 3.6x10<sup>6</sup> J/kg • K