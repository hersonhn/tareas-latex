## Ejercicio 17.57

Se abre la puerta de un refrigerador, y el aire a temperatura ambiente (20.0ºC) llena el compartimiento de 1.50 m<sup>3</sup>. Un pavo de 10.0 kg, también a temperatura ambiente, se coloca en el interior de refrigerador y se cierra la puesta. La densidad del aire es de 1.20 kg/m<sup>3</sup> y su calor específico es de 1020 J/kg • K. Suponga que el calor específico de un pavo, al igual que el del ser humano, es de 2480 J/kg • K.

**¿Cuánto calor debe eliminar el refrigerador de su compartimiento para que el aire y el pavo alcancen el equilibrio térmico a una temperatura de 5.00ºC? Suponga que no hay intercambio de calor con el ambiente circundante.**


### Aire

$\Delta T_a = 20 - 5 = 15$  
$m_a = (1.50 \ m^3)(1.20 \ kg/m^3) = 1.8 \ kg$  
$c_a = 1.007$

$Q_a = m_a c_a \Delta T_a$  
$Q_a = (1.8)(1)(15)$  
$Q_a = 27$

### Pavo

$\Delta T_p = 20 - 5 = 15$  

$Q_p = m_h c_h \Delta T_h$  
$Q_p = (10)(2480)(15)$  
$Q_p = 3.72x10^6$

### Total

$Q = Q_a + Q_p$  
$Q = 27 + 3.72x10^6$   
$Q = 3.72x10^6$



