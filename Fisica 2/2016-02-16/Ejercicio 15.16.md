## Ejercicio 15.16

¿Con qué tensión debe estirarse una cuerda de 2.50 m de longitud y masa de 0.120 kg, para que ondas transversales con frecuencia de 40.0 Hz tengan una longitud de onda de 0.750 m?

$L = 250 m$  
$m = 0.120 kg$  
$f = 40.0 Hz$  
$λ = 0.750$  

$v = \sqrt \frac{T}{\mu} \ \ \ \ \mu = \frac{m}{L} \ \ \ \ v = λ f$

- - -

$v = \sqrt \frac{T}{\mu}$

$λ f = \sqrt \frac{T}{\frac{m}{L}}$

$λ f = \sqrt \frac{TL}{m}$

$(λ f)^2 = \frac{TL}{m}$

$T = \frac{m(λ f)^2}{L}$

$T = \frac{(0.12)((0.75)(40))^2}{250}$

$T = 0.432 N$


### Respuesta

La tensión necesaria es de 0.432 Newtons
