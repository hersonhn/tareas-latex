## Ejercicio 15.12


Demuestre que la ecuación

$y(x, t) = A \cos[ w(\frac{x}{v} - t)] = A \cos[2 \pi f(\frac{x}{v} - t)]$

Puede escribirse como

$y(x, t) = A \cos[\frac{2 \pi}{\lambda}(x - vt)]$


### Demostración


$y(x, t) = A \cos[2 \pi f(\frac{x}{v} - t)]$

$y(x, t) = A \cos[\frac{2 \pi f}{v}(x - vt)]$

$y(x, t) = A \cos[\frac{2 \pi}{\frac{v}{f}}(x - vt)]$ 

$y(x, t) = A \cos[\frac{2 \pi}{\lambda}(x - vt)]$

- - -

Utilice $y(x, t)$ para obtener una expresión para la velocidad trasversal $v_y$ de una partícula de la cuerda en la que viaja la onda.

### Transformación

$\int A \cos[ w (\frac{x}{v} - t) ] \frac{\delta y}{\delta t}$

$-A \sin[ w (\frac{x}{v} - t) ] \int w (\frac{x}{v} - t) \frac{\delta y}{\delta t}$

$-A \sin[ w (\frac{x}{v} - t) ] (\int w \frac{x}{v} \frac{\delta y}{\delta t} - \int wt \frac{\delta y}{\delta t})$

$-A \sin[ w (\frac{x}{v} - t) ] (-w)$

$Aw \sin[ x \frac{w}{v} - wt ]$

$Aw \sin[ x (\frac{vk}{v} - t) ]$  $\ \ \ \ \ $ <sub>(w = vk)</sub>

$Aw \sin[ x (\frac{vk}{v} - t) ]$

- - -

Calcule la rapidez máxima de una partícula de una cuerda.

$MAX \left[ Aw \sin( x (\frac{vk}{v} - t) ) \right]$


$Y_{max} = wA$



