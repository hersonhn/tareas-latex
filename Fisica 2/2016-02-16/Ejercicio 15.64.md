## Ejercicio 15.64


**Energía en un pulso triangular.** Un pulso triangular en una cuerda tensada que viaja en una dirección $+x$ con una rapidez $v$. La tensión de la cuerda es $F$ y la densidad lineal de masa de la cuerda es $\mu$. En $t= 0$, la forma del pulso está dado por

- $0$ sí $x < -L$
- $h(L + x) / L$ para $-L < x < 0$
- $h(L - x) / L$ para $0 < x < L$
- $0$ para $x > L$

1. Dibuje la pulsación en $t = 0$.
2. Determine la función de onda $y(x, t)$ en todos los instantes $t$.
3. Calcule la potencia instantánea de la onda. Demuestre que la potencia es cero excepto cuando $-L < (x - vt) < L$ y que es la constante de este intervalo. Determine el valor de esta potencia constante.

#### Dibuje la pulsación en $t = 0$

![](img1.png)

#### Determine la función de onda $y(x, t)$ en todos los instantes $t$

- $0$ sí $(x - vt) < -L$
- $\frac{h(L + x - vt)}{L}$ para $-L < (x - vt) < 0$
- $\frac{h(L - x + vt)}{L}$ para $0 < (x - vt) < L$
- $0$ para $(x - vt) > L$


#### Demuestre que la potencia es cero excepto cuando $-L < (x - vt) < L$

$P = -F_T \frac{\delta y}{\delta x}\frac{\delta y}{\delta t}$

Potencia para $0$

> $P = -F_T (0)(0) = 0$

Potencia para $\frac{h(L + x - vt)}{L}$

> $P = -F_T \left( \frac{h(L + x - vt)}{L} \frac{\delta y}{\delta x} \right) \left( \frac{h(L + x - vt)}{L} \frac{\delta y}{\delta t} \right)$

> $P = -F_T \left( \frac{h}{L} \right) \left( \frac{-hv}{L} \right)$

> $P = -F_T -\left( \frac{h^2v}{L^2} \right)$

> $P = F_T \frac{h^2v}{L^2}$


Potencia para $\frac{h(L - x + vt)}{L}$

> $P = -F_T \left( \frac{h(L - x + vt)}{L} \frac{\delta y}{\delta x} \right) \left( \frac{h(L - x + vt)}{L} \frac{\delta y}{\delta t} \right)$

> $P = -F_T \left( \frac{-h}{L} \right) \left( \frac{hv}{L} \right)$

> $P = -F_T -\left( \frac{h^2v}{L^2} \right)$

> $P = F_T \frac{h^2v}{L^2}$