### Ejercicio 2

Un cubo de hielo se toma del congelador cuando está a -8.5ºC y se coloca en un calorímetro de aluminio de 95 g lleno con 310 g de agua a temperatura ambiente de 20.0ºC. Se observa que la situación final es sólo agua a 17.0ºC. ¿Cuál fue la masa del cubo de hielo?

**Hielo**

$m_H = ?$  
$T_H = -8.5ºC$  
$c_H = 2093 \ J/gK$  

**Hielo (Fisión)**

$m_H = ?$  
$L_H = 3.33x10^5$  

**Calorímetro**

$m_{Al} = 95g$  
$T_{Al} = 20ºC$  
$c_{Al} = 921$  

**Agua**

$m_a = 310g$  
$T_a = 20ºC$  
$c_a = 4186 \ J/g$  

**Balance**

$T_F = 17ºC$    

#### Procedimiento

$m_H c_H (T_F - TH) + m_H L_H = m_{Al} c_{Al} (T_F - T_{Al}) - m_a c_a (T_F - T_a) = 0$

$m_H (c_H (T_F - TH) + L_H) = m_{Al} c_{Al} (T_F - T_{Al}) - m_a c_a (T_F - T_a) = 0$

$m_H = \frac{m_{Al} c_{Al} (T_F - T_{Al}) - m_a c_a (T_F - T_a) = 0}{c_H (T_F - TH) + L_H}$

$m_H = \frac{(95)(921)(3) + (310)(4186)(3)}{(2093)(25.5) + (3.33x10^5)}$

$m_H = 10.75g$