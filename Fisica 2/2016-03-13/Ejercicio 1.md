### Ejercicio 1

Un cubo de hielo de 30g en su punto de fusión se sueltan en un contenedor aislado de nitrógeno líquido se suelta en un contenedor aislado de nitrógeno líquido. ¿Cuánto nitrógeno se evapora si este está en su punto de ebullición de 77 K y tiene un calor latente de vaporización de 220 J/gK?


$\Delta T_H = 0ºC = 273.15 K$  
$m_H = 30 g$  
$c_H = 2.09 \ J/gK$

$T_N = 77 K$  
$m_N = \ ?$  
$L_N = 220 \ J/gK$

$Q = m_H c_H \Delta T_H$  
$Q = m_N L_N$

$m_H c_H \Delta T_H = m_N L_N$

$m_N = \frac{m_H c_H \Delta T_H}{L_N}$

$m_N = \frac{(2.09) (30) (273.15 - 77)}{220}$

$m_N = 55.9$

