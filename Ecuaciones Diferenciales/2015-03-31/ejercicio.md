## Ejercicio

Encuentre la solución particular de la ecuación diferencial $\frac{1}{4}y'' + y' + y = x^2 - 2x$

- - - 

$\frac{1}{4}y'' + y' + y = x^2 - 2x$

$y'' + 4y' + 4y = 4x^2 - 8x$

$m^2 + 4m + 4$

$(m + 2)(m + 2)$

m_1 = m_2 = -2

$Y_c = C_1e^{-2x} + C_2xe^{-2x}$



$[x^2 - 2x]$

$y = $

$y' = 2Ax - B$

$y'' = 2A$ 

$(2A) + 4(2Ax - B) + 4(Ax^2 - Bx) = $x^2 - 2x$

$2A + 8Ax -4B + 4Ax^2 - 4B x + C = x^2 - 2x$

$(4Ax^2) + (8Ax - 4Bx) + (2A - 4B) = x^2 - 2x$

> $4A = 1$

> $8A - 4B = -2$

> $8A - 4B + 4C = 0$

$4A = 1$

$A = \frac{1}{4}$

$8A - 4B = -2$

$8(\frac{1}{4}) - 4B = -2$

$2 - 4B = -2$

$4B = -4$

$B = -1$

$8(\frac{1}{4}) - 4(-1) + 4C = 0$

$C = \frac{7}{8}$

$Yp = \frac{1}{4}x^2 + x + \frac{7}{8}$

$Y = C_1e^{-2x} + C_2xe^{-2x} + \frac{1}{4}x^2 + x + \frac{7}{8}$

