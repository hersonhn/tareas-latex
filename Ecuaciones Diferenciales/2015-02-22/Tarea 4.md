

## Tarea #4

Se aplica una fuera electromotriz aun circuito en serie en el que la resistencia es de 200 ohms y la capacitancia es de $10^{-4}$ farads.  Encuentre la carga $q(t)$ en el capacitor si $q(0) = 0$. Encuentre la corriente $i(t)$.


$E(t) = V_R + V_C$

$E(t) = Ri + \frac{q}{c}$

$E(t) = R\frac{dq}{dt} + \frac{q}{c}$

$100 = 200 \frac{dq}{dt} + 10^{4}q$

$0.5 = \frac{dq}{dt} + 50 q$

$\frac{dq}{dt} + 50q = 0.5$

$P(t) = 50$

- - -

$e^{\int P(t) dt} = e^{50t}$

$e^{50t}\ dq + e^{50t} 50q \ dt = e^{50t} 0.5\ dt$


$\frac{dq}{dt} = \frac{e^-50t}{2}$

> $i = \frac{dq}{dt}$

$i = \frac{e^-50t}{2}$

- - -

$e^{50t}\ dq + e^{50t} 50q \ dt = e^{50t} 0.5\ dt$

$\int (e^{50t} q)' = \int e^{50t} 0.5\ dt$

$e^{50t}q = \frac{1}{100}e^{50t} + C$

$q(t) = \frac{1}{100} + ce^{-50t}$

> $t = 0$

$q(0) = \frac{1}{100} + ce^{0}$

$0 = \frac{1}{100} + c$

$c = -\frac{1}{100}$

$q(t) = -\frac{1}{100} - \frac{1}{100} e^{50t}$



