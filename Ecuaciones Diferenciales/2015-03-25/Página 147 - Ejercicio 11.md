Página 147 - Ejercicio 11
-------------------------

$y'' + 4y' + 5y = 0$

$m^2 + 4m + 5 = 0$

$m = \frac{-b ± \sqrt{b^2 - 4bc}}{2a}$

$m = \frac{-4 ± \sqrt{4^2 - (4)(5)}}{2}$

$m = \frac{-4 ± \sqrt{16 - 20}}{2}$

$m = -2 ± \frac{\sqrt{-4}}{2}$

$m = -2 ± \frac{2i}{2}$

$m = -2 ± i$

$Y = C_1 e^{(-2 + i)x} + C_2 e^{(-2 - i)x}$

