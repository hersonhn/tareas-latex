## Ejercicio 2

Un termometro que marca 70º F se coloca en un horno precalentado a una temperatura constante.  Por una ventana de vidrio en la puerta del horno, un observador registra que después de medio minuto el termometro marca 110º F y luego de un minuto la lectura es de 145º F

¿Cuál es la temperatura del horno?

$T(t) = ce^{kt} + Tm$

$70 = ce^{0k} + Tm$

$110 = ce^{30k} + Tm$

$145 = ce^{60k} + Tm$

$70 = c + Tm$

$110 = ce^{30k} + 70 - c \ \ \ \{-1\}$

$145 = ce^{60k} + 70 - c$

$35 = ce^{60k} - ce^{30k}$

$c = \frac{35}{ ce^{60k} - ce^{30k} }$