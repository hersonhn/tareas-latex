## Ejercicio 1

La población de bacterias en un cultivo crece a un tasa proporcional al número de bacterias presentes en el tiempo $t$. Después de tres horas se observó que están presentes 400 bacterias. Después de diez horas hay 2000 bacterias. ¿Cuál fue el número inicial de bacterias?

$P(0) = ?$

$P(3) = 400$

$P(10) = 2000$

$2000 = 400e^{7k}$

$\frac{2000}{400} = e^{(10 - 3)k}$

$\ln 5 = 7k$

$k = 0.2299$

- - -

$400 = x e^{3k}$

$400 = x e^{3(0.2299)}$

$x = \frac{400}{e^{3(0.2299)}}$

$x = 200.69$

**Respuesta:**

La población inicial es del 200 bacterias.

