## Ejericico 2

Verificar si $y = \frac{1}{(1 - \sin x)^{-\frac{1}{2}}}$ es solución de $2y' = y^3 \cos x$

$y = \frac{1}{(1 - \sin x)^{-\frac{1}{2}}}$

$y = ((1 - \sin x)^{-\frac{1}{2}})^{-1}$

$y = (1 - \sin x)^{\frac{1}{2}}$

$y' = \frac{d}{dx}(1 - \sin x)^{\frac{1}{2}}$

**Comprobación:**

$2y' = y^3 \cos x$

$2\left(\frac{1}{2} (1 - \sin x)^{-\frac{1}{2}} (- \cos x) \right) = \left( \frac{1}{(1 - \sin x)^{-\frac{1}{2}}} \right)^3 \cos x$

$(1 - \sin x)^{-\frac{1}{2}} (- \cos x) = \frac{1}{(1 - \sin x)^{-\frac{1}{2}}} \cos x$


$(1 - \sin x)^{-\frac{1}{2}} (- \cos x) = (1 - \sin x)^{\frac{1}{2}(3)} \cos x$

$-(1 - \sin x)^{-\frac{1}{2}} \cos x = (1 - \sin x)^{\frac{3}{2}} \cos x$

**Respuesta:**

No es solución
