## Ejericico 1

Verificar si $y = \frac{1}{4 - x^2}$ es solución de $y' = 2xy^2$


$y = (4 - x^2)^{-1}$


$y' = \frac{d}{dx}(4 - x^2)^{-1}$

$y' = -(4 - x^2)^{-2}(-2x)$

$y' = (4 - x^2)^{-2}(2x)$

$y' = \frac{2x}{(4 - x^2)^2}$

**Verificación:**

$y = \frac{1}{4 - x^2}$

$\frac{2x}{(4 - x^2)^2} = 2x\left(\frac{1}{4 - x^2}\right)^2$

$\frac{2x}{(4 - x^2)^2} = \frac{2x}{(4 - x^2)^2}$

**Respuesta:**

Es Solución