Una masa que pesa 24 libras unidas al extremo de un resorte alarga a éste 4 pulgadas, al inicio la masa se libera desde el reposo en un punto 3 pulgadas arriba del punto de equilibrio. Encuentre la ecuación de movimiento.

$p = 24\ lb$

$m =\frac{24\ lb}{32 \frac{ft}{s^2}} = 0.75\ slug$

$s = 4\ in = \frac{1}{3}\ ft$

- - -

$ks = mg$

$ks = (0.75)(32)$

$ks = 24$

$k = \frac{24}{\frac{1}{3}} = (24)(3) = 72$

$k = 72$

- - - 

$w^2 = \frac{k}{m}$

$w = \sqrt \frac{k}{m}$

$w = \sqrt \frac{72}{0.75}$

$w = 9.79$

- - - 

#### Ecuación inicial


$- \frac{1}{4} = C_1 \cos{ 9.79t} + C_2 \sin{ 9.79t }$

#### Encontrando $C_1$

$- \frac{1}{4} = C_1(1) + C_2(0)$

$C_1 = -\frac{1}{4}$

#### Econtrando $C_2$

$0 = (C_1 \cos{ 9.79t } + C_2 \sin{ 9.79t })\ dt $

$0 = -C_1 9.79 \sin { 9.79t } + C_2 9.79 \cos{ 9.79t }$

$0 = -C_1 9.79 (0) + C_2 9.79 (1)$

$0 = C_2 9.79$

$C_2 = \frac{0}{9.79}$

$C_2 = 0$

#### Ecuación final

$-\frac{1}{4} = -\frac{1}{4} \cos{ 9.79t }$

$1 = \cos{ 9.79t }$

$\cos{ 9.79t } - 1 = 0$