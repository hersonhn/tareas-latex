## Página 283 

### Ejercicio 28

$f(t) = t^2 - e^{-9t} + 5$

$\mathscr{L}[f(t)] = \mathscr{L}[t^2] - \mathscr{L}[ e^{-9t}] + \mathscr{L}[5]$

$F(s) = \frac{2}{s^3} - \frac{1}{s + 9} + \frac{5}{s}$

### Ejercicio 29

$f(t) = (1 + e^{2t})^2$

$f(t) = 1 + 2e^{2t} + e^{4t}$

$\mathscr L[f(t)] = \mathscr L[1] + 2\mathscr L[e^{2t}] + \mathscr L[e^{4t}]$

$F(s) = \frac{1}{s} + 2\frac{1}{s - 2} + \frac{1}{s - 4}$

$F(s) = \frac{1}{s} + \frac{2}{s - 2} + \frac{1}{s - 4}$