## Página 292 

### Ejercicio 3

$\mathscr L^{-1} \left[ \frac{1}{s^2} - \frac{48}{s^5} \right]$

$\mathscr L^{-1} \left[ \frac{1}{s^2} \right] - 48\mathscr L^{-1} \left[ \frac{1}{s^5} \right]$

$= t - 48\frac{t^4}{24}$

$= t - 2t^4$


### Ejercicio 10

$\mathscr L^{-1} \left[ \frac{2s - 6}{s^2 + 9} \right]$

$\mathscr L^{-1} \left[ \frac{2s}{s^2 + 9} \right] - \mathscr L^{-1} \left[ \frac{6}{s^2 + 9} \right]$

$2\mathscr L^{-1} \left[ \frac{s}{s^2 + 9} \right] - 6\mathscr L^{-1} \left[ \frac{1}{s^2 + 9} \right]$

$2\cos{3t} - 6\frac{\sin 3t}{3}$

$2\cos{3t} - 2\sin{3t}$