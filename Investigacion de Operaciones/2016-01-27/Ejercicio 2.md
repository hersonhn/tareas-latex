### Ejercicio # 2 <small>(Plan de Embarque/Mezcla de Productos)</small>

Gasahol, Inc. tiene 14,000 galones de una mezcla de gasolina y alcohol almacenada en su instalación de Fresno y 16,000 galones almacenados en su instalación de Bakersfield. Desde las instalaciones, Gasahol debe proveer a Fresh Food Farmer 10,000 galones y a American Grovers 20,000 galones. El costo de embarcar 1 galón desde cada instalación de almacenado a cada cliente es:

| De          | Fresh Food | American Grover |  
| ----------- | ---------- | --------------- |  
| Fresno      | \$0.04     | \$0.06          |  
| Bakersfield | \$0.05     | \$0.03          |  

**Formule un modelo de programación lineal para determinar el plan de embarque de costo mínimo que satisfaga las restricciones de provisión y demanda.**

### Variables

a = de Fresno a Fresh Food *(galones)*
b = de Fresno a American Grover *(galones)*
c = de Bakersfield a Fresh Food *(galones)*
d = de Bakersfield a American Grover *(galones)*

### Función Objetivo

$CostoMínimo = 0.04a + 0.06b + 0.05c + 0.03d$

### Restricciones

$a + b = 14000$
$c + d = 16000$
$a + c = 10000$
$b + d = 20000$

### Restricciones Lógicas

$a, b, c, d > 0$