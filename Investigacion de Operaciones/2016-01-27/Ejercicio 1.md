### Ejercicio # 1 <br/> <sup>(Plan de Dieta)</sup>

Una carnicería acostumbra preparar carne para hamburguesa con una combinación de carne molida de res y carne de cerdo. La carne de res contiene 80% de carne y 20% de grasa y le cuesta 80 centavos la libra.

La carne de cerdo contiene 68% de carne y 32% de grasa y cuesta 60 centavos por libra **¿Qué cantidad de cada tipo de carne debe emplear la tienda por cada libra de carne para hamburguesa si desea minimizar el costo y mantener el contenido de grasa no mayor del 25%?**

### Variables 

R = Cantidad de carne de res.
C = Cantidad de carne de cerdo.

### Función Objetivo

$MinZ = 0.8R + 0.6C$

### Restricciones

Cantidad de grasa: $\frac{0.2R + 0.32C} {2(R + C)} ≤ 0.25$

### Restricciones Lógicas

$R, C > 0	$