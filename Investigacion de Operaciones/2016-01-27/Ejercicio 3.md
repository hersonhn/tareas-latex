
### Ejercicio # 3 <br/> <sup>(Plan de transporte)</sup>

Un destacamento militar formado por 50 soldados ingenieros, 36 zapadores, 22 de las fuerzas especiales, y 120 soldados de infantería como tropa de apoyo, ha de transportarse hasta una posición estratégica importante. En el parque de la base se dispone de 4 tipos de vehículos, A, B, C y D, acondicionados para transporte de tropas. El número de personas que cada vehículo puede transportar se detalla en la siguiente tabla:


|     | Ingenieros | Zapadores | Fuerzas Especiales | Infantería |
| --- | ---------- | --------- | ------------------ | ---------- |
|  A  | 3          | 2         | 1                  | 4          |
|  B  | 1          | 1         | 2                  | 3          |
|  C  | 2          | 1         | 2                  | 1          |
|  D  | 3          | 2         | 3                  | 1          |


El combustible necesario para que cada vehículo llegue hasta el punto dedestino se estima en 160, 80, 40 y 120 galones respectivamente. **Si se quiere ahorrar combustible, ¿cuántos vehículos de cada tipo habrá que utilizar para que el consumo sea lo mínimo posible?**

### Variables

a = cantidad de vehículos de tipo A
b = cantidad de vehículos de tipo B
c = cantidad de vehículos de tipo C
d = cantidad de vehículos de tipo D

### Función Objetivo

$MinC = 160a + 80b + 40c + 120d$

### Restricciones

Ingenieros: $a3 + b + 2c + 3d ≥ 50$
Zapadores: $2a + b + c + 2d ≥ 36$
Fuerzas Esp: $a + 2b + 2c + 3d ≥ 22$
Infantería: $3a + 2b + 3c + d ≥ 120$

### Restricciones Lógicas

$a, b, c, d > 0$