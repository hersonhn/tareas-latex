## Ejercicio #1
Un herrero desea construir bicicletas de paseo y de montaña. Cuenta con 80 kg de acero y 120 kg de aluminio. La bicicleta de paseo la venderá a 200 y la bicicleta de montaña a 150. Par la bicicleta de paseo utiliza 1 kg de acero y 3 kg de aluminio, y para la bicicleta de montaña utiliza 2 Kg. de ambos metales.

¿Cuantas bicicletas de cada tipo debe vender para maximizar su ganancia?

### Maximizar ganancias

|  Material        | Bicicleta de Paseo | Bicicleta de Montaña | Total |
| ---------------- | ------------------ | -------------------- | ----- |
| Acero            | 1                  | 2                    | 80    |
| Aluminio         | 3                  | 2                    | 120   |
| **Precio Venta** | 200                | 150                  |       |


### Variables

p = Cantidad de bicicletas de paseo.  
m = Cantidad de bicicletas de montaña.


### Función Objetivo

$MaxZ = 200p + 150m$


### Restricciones

- Cantidad de aluminio: $1p + 2m ≤ 80$
- Cantidad de acero: $3p + 2m ≤ 120$

### Restricciones Lógicas

- $p > 0$
- $m > 0$

- - - 

### Procedimiento

**Aluminio:**

$p = 0$  
$1(0) + 2m = 80$  
$2m = 80$  
$m = 40$  
$(0, 40)$

$m = 0$  
$1p + 2(0) = 80$  
$p = 80$  
$(80, 0)$

**Acero:**

$p = 0$  
$3(0) + 2m = 120$  
$2m = 120$  
$m = 60$  
$(0, 60)$

$m = 0$  
$3p + 2(0) = 120$  
$3p = 120$  
$p = 40$

![](graph1.png)


### Puntos

**Punto A:** (0, 40)

**Punto B:**

$p + 2m = 80$ (-1)  
$3p + 2m = 120$  

$p + 2m = 80$  
$\underline{3p + 2m = 120}$  
$2p = 40$  
$p = 20$

$20 + 2m = 80$  
$2m = 60$  
$m = 30$

**Punto B:** (20, 30)

**Punto C:** (40, 0)

### Substituciones

- Punto A = (0, 40)

    $z = 200p + 150m$  
    $z = 0 + 150(40)$  
    $z = 6000$

- Punto B = (20, 30)

    $z = 200p + 150m$  
    $z = 200(20) + 150(40)$  
    $z = 4000 + 4500$  
    $z = 8500$

- Punto C = (40, 0)

    $z = 200p + 150m$  
    $z = 200(40) + 0$  
    $z = 8000$

### Respuesta

La solución óptima es 20 bicicletas de paseo y 30 de montaña.

