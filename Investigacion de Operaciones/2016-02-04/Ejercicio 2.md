## Ejercicio #2
Un estudiante dedica parte de su tiempo libre a repartir propaganda publicitaria. La empresa A le paga &dollar;5 por cada impreso repartido y la empresa B, con folletos mas grandes le paga &dollar;7 por impreso. El estudiante lleva dos bolsas: Una para los impresos de la empresa A, en la que le caben 120 y otra bolsa para los impresos de la empresa B, en la que caben 100. No puede repartir mas de 150 impresos.

¿Cuantos impresos tendrá que repartir de cada clase para maximizar su ganancia?

### Maximizar ganancias

### Variables

a = Cantidad de folletos de la empresa A.  
b = Cantidad de folletos de la empresa B.### Función Objetivo

$MaxZ = 5a + 7b$

### Restricciones

a) $A ≤ 120$  
b) $B ≤ 150$  
c) $A + B ≤ 150$  
d) $A > 0$  
e) $A > 0$  

### Puntos

①  = (120, 0)  
④  = (0, 100)


**Punto ②**  

$a = 120$  
$a + b = 130$  
$b = 30$

② = (120, 30)

**Punto ③**

$b = 100$  
$a + b = 150$  
$a = 50$  

③ = (50, 100)


![](graph2.png)


### Evaluar puntos

$(5a + 7b)$

① $\to 5(120) + 7(0) = 600$  
② $\to 5(120) + 7(30) = 600 + 210 = 810$  
③ $\to 5(50) + 7(100) = 250 + 7(100) = 950$  
④ $\to 5(0) + 7(100) = 700$ 


### Respuesta 

La solución optima es repartir 50 folletos de la empresa A y 100 folletos de la empresa B.
