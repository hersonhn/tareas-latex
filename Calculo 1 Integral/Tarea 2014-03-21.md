
# TAREA DE CÁLCULO 1 DIFERENCIAL
### HERSON ARON SALINAS LÓPEZ
#### 21 de marzo de 2014

- - - -

<div style="page-break-before: always;"></div>


## Ejercicios con Límites

- Ejercicio A

$$\lim_{\substack{x \to 1}} \frac{\ln{x} }{x - 1}$$
$$\frac{
    \lim_{\substack{x \to 1}} \ln{x}
}{
    \lim_{\substack{x \to 1}} x - 1
}$$
$$
    \frac{\frac{d}{dx}\ \ln{x} }{ \frac{d}{dx}\ \ x - 1 }  =
    \frac{\frac{1}{x} }{ 1 } =
    \frac{1}{x} = 1
$$

- Ejercicio B

$$\lim_{\substack{x \to 0}} \frac{ e^x - \cos{x} }{ \sin{x} }$$

$$
    \lim_{\substack{x \to 0}} \frac{ e^x - \cos{x} }{ \sin{x} } = 
    \frac{ e^1 - \cos{1} }{ \sin{1} } = 98.46
$$


- Ejercicio C

$$\lim_{\substack{x \to 0^+}}  (1 + x)^{\ln{x}}$$

$$
\frac{
    \frac{d}{dx}\ 1 + x
}{
    \frac{d}{dx}\ \ln{x}
} = 
\frac{
    \frac{1}{x}
}{
    1
} = 
\infty
$$

- Ejercicio D

$$\lim_{\substack{x \to \infty}} \frac{\ln{x}}{x}$$

$$
\frac{
    \frac{d}{dx}\ \ln{x}
}{
    \frac{d}{dx}\ x
} =
\frac{
    \frac{1}{x}
}{
    1
} =
\frac{1}{x} = 0
$$

<div style="page-break-before: always;"></div>

- Ejercicio E

$$\lim_{\substack{x \to \infty}} (e^x + x)^{2/x}$$

$$
\ln{Y'} = \frac{
    \frac{d}{dx}\ {2/x}
}{
    \frac{d}{dx}\ e^x + x
}
$$

$$
\ln{Y'} = \frac{
    {2/x^2}
}{
    e^x + x
}
$$

$$
\ln{Y'} = \frac{
    2
}{
    (e^x + x)\ (x^2)
} = 
\frac{
    2
}{
    (\infty)(\infty)
} = \frac{2}{\infty} = 0
$$

$$\ln{Y'} = 0$$

$$Y = e^0 = 1$$

$$\lim_{\substack{x \to \infty}} (e^x + x)^{2/x} = 1$$

- Ejercicio F

$$
    \lim_{\substack{x \to \infty}} 
    \frac{ x^2 + 2x }{ e^{3x} - 1 }
$$

$$
    \frac{
        \frac{d}{dx}\ x^2 + 2x
    }{
        \frac{d}{dx}\ e^{3x} - 1
    }
$$

$$
\frac{
        2x + 2
    }{
        e^{3x}
    }
    =
\frac{2}{3e^\infty} = \frac{2}{\infty} = 0
$$

- Ejercicio G

$$\lim_{\substack{x \to 2}}\ \ (x - 2) \tan{\frac{\pi x}{4}}$$

$$
(2 - 2) \tan{\frac{\pi 2}{4}}\ \ =\ \ 
0\ \tan{\frac{\pi}{2}}\ \  =\ \ 
0
$$

<div style="page-break-before: always;"></div>

- Ejercicio H

$$\lim_{\substack{x \to \infty}}\ \  \left(1 + \frac{1}{2x} \right)^{2x}$$

$$
\ln{Y'} = 
\frac{
    \frac{d}{dx}\ x^2
}{
    \frac{d}{dx}\ 1 + \frac{1}{2x}
}
=
\frac{2x}{\frac{1}{2x^2}}
=
4x^3
$$

$$
\ln{Y'} = 4x^3
$$

$$
\ln{Y'} = 4\infty^3
$$

$$
\ln{Y'} = \infty
$$

$$
Y' = e^\infty
$$

$$
Y' = \infty
$$

$$
\lim_{\substack{x \to \infty}}\ \  \left(1 + \frac{1}{2x} \right)^{2x}
= \infty
$$


<div style="page-break-before: always;"></div>


## Ejercicios con Integrales Simples

- Ejercicio A

$$\int (x^3 + 3x^2 - 2x + 3)\ dx $$
$$
\frac{1}{3 + 1} x^{3+1} +
3\frac{1}{2 + 1} x^{2+1} -
2\frac{1}{1 + 1} x^{1+1} +
3\frac{1}{0 + 1} x^{0+1} + C
$$
$$
\frac{1}{4} x^{4} +
3\frac{1}{3} x^{3} -
2\frac{1}{2} x^{2} +
3\frac{1}{1} x^{1} + C
$$
$$\frac{1}{4} x^{4} + x^{3} - x^{2} + 3x + C$$

<div style="page-break-before: always;"></div>

- Ejercicio B

$$\int\ x\ (\sqrt{x} - \sqrt[3]{x})\ \ dx $$
$$x\ x^{1/2} - x\ x^{1/3} + C$$
$$x^{3/2} - x^{4/3} + C$$
$$
\frac{1}{\frac{3}{2} + 1} x^{\frac{3}{2}+1} -
\frac{1}{\frac{4}{3} + 1} x^{\frac{4}{3}+1} + C
$$
$$
\frac{1}{5/2} x^{5/2} -
\frac{1}{7/3} x^{7/3} + C
$$
$$
\frac{2}{5} x^{5/2} -
\frac{3}{7} x^{7/3} + C
$$

<div style="page-break-before: always;"></div>

- Ejercicio C

$$\int\ \frac{(x^2 + 1)^2}{\sqrt{x}} dx $$
$$
\int\ 
\frac{x^4}{\sqrt{x}} +
\frac{2x^2}{\sqrt{x}} +
\frac{1}{\sqrt{x}} + C
\ \ \ \ dx
$$
$$
\int\ 
x^4\ x^{-1/2} +
2x^2\ x^{-1/2} +
x^{-1/2} + C
\ \ \ \ dx
$$
$$
\int\ 
x^{7/2} +
2x^{3/2} +
x^{-1/2}
\ \ \ dx
$$
$$
\frac{1}{\frac{7}{2} + 1} x^{\frac{7}{2} + 1} +
2\frac{2}{\frac{3}{2} + 1} x^{\frac{3}{2} + 1} +
x^{- \frac{1}{2}} + C
$$
$$
\frac{1}{\frac{9}{2}} x^{9/2} +
2\frac{2}{\frac{5}{2}} x^{5/2} +
x^{-1/2} + C
$$
$$
\frac{2}{9} x^{9/2} +
2\frac{2}{5} x^{5/2} +
x^{-1/2} + C
$$
$$
\frac{2}{9} x^{9/2} +
\frac{4}{5} x^{5/2} +
x^{-1/2} + C
$$
