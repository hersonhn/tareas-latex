TAREA DE CÁLCULO 1 DIFERENCIAL 

### HERSON ARON SALINAS LÓPEZ 
#### 1 de Junio del 2014 
- - - - 
<div style="page-break-before: always;"></div> 

# Sección 6.1
## Ejercicio 5


$y = 2 -x^2$
$y = x$

**Gráfica**

<img src="https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-xpf1/v/t34.0-12/10416890_1497939017085100_33106221_n.jpg?oh=1116ae9d71265ec098c6d48b01dd6e94&oe=538E382F&__gda__=1401802162_aa41a964b71ae09629a9939483981608" width="400">

$y = 2 -x^2 = x$

$2 -x^2 = x$

$2 -x^2 - x = 0$

$x^2 + x + 2 = 0$

$(x + 2) (x - 1)= 0$

$x = \{ -2, 1 \}$

**Puntos:** $(-2, -2) (1, 1)$

$\\$

$\int_{-2}^1 (2 - x^2) - x\ \ dx $

$-\frac{1}{3}x^3 -\frac{1}{2}x^2 + 2x\ \bigg|_{-2}^1$

$(-\frac{1}{3}(-2)^3 -\frac{1}{2}(-2)^2 + 2(-2)) - (-\frac{1}{3} -\frac{1}{2} + 2)$

### $ \frac{7}{2}$ unidades$^2$


- - - - 
<div style="page-break-before: always;"></div> 

## Ejercicio 14

$y = x^2 - 4x - 5$
$y = 0$
entre $x = -1$, $x = 4$

**Gráfica:**

<img src="https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-xpa1/v/t34.0-12/10417108_1497941310418204_1335084354_n.jpg?oh=ece3d53fdc51bd715dfe84ad37e01adc&oe=538E52BD&__gda__=1401808364_7925da14e9b0fb7e917d4efda33854c1" width="400">

$\int_{-1}^4 0 - (x^2 - 4x - 5)\ \ dx $

$-\frac{1}{3}x^3 -4\frac{1}{2}x^2 + 5x\ \bigg|_{-1}^4$

$(-\frac{1}{3}4^3 -4\frac{1}{2}4^2 + 5(4)) - (-\frac{1}{3}(-1)^3 -4\frac{1}{2}(-1)^2 + 5(-1))$

$-\frac{72}{3}$

### $24$ unidades$^2$



- - - - 
<div style="page-break-before: always;"></div> 

## Ejercicio 21

$y = x^2 - 2x$
$y = -x^2$

**Gráfica:**

<img src="https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-xpf1/v/t34.0-12/10405760_1497942390418096_1033185441_n.jpg?oh=ac7fbcc17c7406dae6c7959dfa7c7bff&oe=538DB8CB&__gda__=1401808895_68915f75a275d68c69be3ee167169cfc" width="400">

$x^2 - 2x = -x^2$

$-2x^2 - 2x = 0$

$x^2 - x = 0$

$(x - 1) x = 0$

$x = \{ 0, 1\}$

**Puntos:** $(0, 0) (1, -1)$

$\int_{0}^1 -x^2 - (x^2 - 2x) \ \ dx $

$\int_{0}^1 -x^2 - x^2 + 2x \ \ dx $

$\int_{0}^1 -2x^2 - 2x \ \ dx $

$-\frac{2}{3}x^3 + x^2 \ \bigg|_{-1}^4$

$(- \frac{2}{3} + 1) + (0 + 0)$

### $\frac{1}{3}$ unidades$^2$

- - - - 
<div style="page-break-before: always;"></div> 

## Ejercicio 25

$x = -6y^2 + 4y$
$x + 3y - 2 = 0$


<img src="https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-xpf1/v/t34.0-12/10419799_1497943367084665_475161380_n.jpg?oh=0432fa1ddba12535cd4d88872ea2cc49&oe=538DE759&__gda__=1401829449_d3b023d9e04c37199906ad9be327b365" width="400">

$-6y^2 + 4y + 3y - 2 = 0$

$-6y^2 + 7y - 2 = 0$

$(3y - 2)(2y - 1) = 0$

$y = \{\frac{1}{2}, \frac{2}{3}\}$


$\int_{\frac{1}{2}}^{\frac{2}{3}} (-6y^2 + 4y) - (-3y + 2) \ \ dx $

$\int_{\frac{1}{2}}^{\frac{2}{3}} -6y^2 + 4y + 3y - 2 \ \ dx $

$\int_{\frac{1}{2}}^{\frac{2}{3}} -6y^2 + 7y - 2 \ \ dx $

${ \frac{-6}{3}y^3 + \frac{7}{2}y^2 - 2y } \ \bigg|_{\frac{1}{2}}^{\frac{2}{3}}$

### $\frac{2}{3}$ unidades$^2$