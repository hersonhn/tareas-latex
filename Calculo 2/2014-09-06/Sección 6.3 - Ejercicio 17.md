### Ejercicio 17

$z = 1 + x + y$
$x = 1$
$y = 0$
$y = x^2$

<img src="https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-xpf1/v/t34.0-12/10613940_1529034143975587_852058015_n.jpg?oh=b6d9275b3d2f9b01253e1560231f3706&oe=540DE6A4&__gda__=1410184500_ab8131727e31b8e116626a7344ba98ea" width="250">

$v = \int^1_0 \int^{x^2}_0 1 + x + y \ dy \ dx$

$= \int^1_0 \int^{\sqrt{y}}_0 1 + x + y \ dx \ dy$

$= \int^1_0 {\big( x + x^2 + yx \ \big|^{\sqrt{y}}_0 \big)} \ dy$

$= \int^1_0 {\big( \sqrt{y} + y + y\sqrt{y}\ \big)} \ dy$

$= \int^1_0 {\big( y^{\frac{1}{2}} + y + y^{\frac{3}{2}}\big)} \ dy$

$= \int^1_0 {  \frac{2}{3}y^{\frac{3}{2}} + \frac{y^2}{2} + \frac{2}{5}y^{\frac{5}{2}}  } \ dy$

$= \frac{2}{3} + \frac{1}{2} + \frac{2}{5}$

$= \frac{20 + 15 + 12}{30}$

$v = \frac{47}{30} unidades^3$
