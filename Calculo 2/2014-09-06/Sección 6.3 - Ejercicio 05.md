## Sección 6.3

### Ejercicio 5

Use integración doble para encontrar el area de la región en el plano XY limitado por las curvas dadas.

$y = x^2$
$x+ y = 2$
$y = 0$

<img src="https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-xpf1/v/t34.0-12/10634267_1529026630643005_66658502_n.jpg?oh=7e7c3ec256685f5b0644c3e24bccf1a8&oe=540DE6FA&__gda__=1410203559_00c69b97903d6c91b9b1ca5e1743c170" width="400">

$\int^1_0\int^{-y+2}_0\ dx\ dy + \int^2_0\int^{\sqrt{y}}_0\ dx\ dy$

$=\int^1_0\big(x\ \big|^{-y+2}_0 \big)\ dy + \int^2_0\big(x\ \big|^{\sqrt{y}}_0\ dx\big)\ dy$

$=\int^1_0\big(-y+2\big)\ dy + \int^2_0\big(\sqrt{y}\ \big)\ dy$

$=\big(-\frac{y^2}{2}+2y\ \big|^1_0\big) + \big(\frac{2}{3}x^{\frac{3}{2}}\ \big|^2_0\big)$

$=\frac{1}{2} + 2 + \big(\frac{2}{3}(2)^\frac{3}{2} -\frac{2}{3}\big)$

$\approx 3.719$