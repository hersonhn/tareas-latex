## Sección 6.3

### Ejercicio 25

$z = 4x^2 + y^2$
$x = 0$
$y = 0$
$2x + y = 2$

<img src="https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-xpa1/v/t34.0-12/10682146_1529038130641855_52373622_n.jpg?oh=697feaf550905c65fa091c8be7562a7e&oe=540E0D0D&__gda__=1410198610_bc2e64f82380fef41d7b2fa7dfc842ee" width="350">

$v = \int^2_0 \int^{1- \frac{y}{2}}_0 4x^2 + y^2 \ dx \ dy$

$= \int^2_0 {\big(  \frac{4}{3}x^3 + y^2x\ \ \big|^{1- \frac{y}{2}}_0  \big)} \ dy$

$= \int^2_0 { \frac{4}{3}(1- \frac{y}{2})^3 + y^2(1- \frac{y}{2}) } \ dy$

$= \int^2_0 { -\frac{y^3}{6} + y^2 -2y + \frac{4}{3} + y^2 - \frac{y^3}{2} } \ dy$

$= \int^2_0 { -\frac{2}{3}y^3 + 2y^2 -2y + \frac{4}{3} } \ dy$

$= -\frac{2}{12}y^4 + \frac{2}{3}y^3 -y^2 + \frac{4}{3}y \ \big|^2_0 $

$= -\frac{2}{12}(2)^4 + \frac{2}{3}(2)^3 - 2^2 + \frac{4}{3}(2) $

$= -\frac{8}{3} + \frac{16}{3} - 4 + \frac{8}{3} $

$=  \frac{16}{3} - 4 $

$=  \frac{16}{3} - \frac{12}{3} $

$v =  \frac{4}{3} unidades^3$