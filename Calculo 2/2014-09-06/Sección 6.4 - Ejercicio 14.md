## Sección 6.4

### Ejercicio 14

$z = 3 + \cos x + \cos y$
$x = 0$, $x = \pi$
$y = 0$, $y = \pi$
- - -
$v = \int^\pi_0 \int^\pi_0 { 3 + \cos x + \cos y }\ dx \ dy$

$= \int^\pi_0 \big({ 3x + \sin x + x\cos y \ \big|^\pi_0 }\big) \ dy$

$= \int^\pi_0 3\pi + \sin \pi + \pi\cos y \ \ dy$

$= \pi\int^\pi_0 3 + \cos y \ \ dy$

$= \pi\big( 3y + \sin y \ \big|^\pi_0 \big)$

$= \pi\ (3\pi + 0)$

$v = 3\pi^2\ unidades^3$