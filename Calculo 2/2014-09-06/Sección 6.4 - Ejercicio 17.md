### Ejercicio 17

$z = 1 + y + x$
$x = 1$
$y = 0$
$y = x^2$

<img src="https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-xpf1/v/t34.0-12/10613911_1529042493974752_868441947_n.jpg?oh=b5863daad963743948a5cbf95cc2c3f3&oe=540DD772&__gda__=1410200418_d0844a08e2e7a578f24ad5fb313f09a4" width="300" >

$v = \int^1_0 \int^{\sqrt y}_0 { 1 + y + x }\ dx \ dy$

$= \int^1_0 \big({ x+ yx + \frac{x^2}{2} \ \big|^{\sqrt y}_0 }\big) \ dy$

$= \int^1_0  {\sqrt y}+ y{\sqrt y} + \frac{y}{2}  \ dy$

$= \int^1_0  y^\frac{1}{2} + y^\frac{3}{2}  + \frac{y}{2}   \ dy$

$= \frac{2}{3}y^\frac{3}{2} + \frac{2}{5}y^\frac{5}{2} + \frac{y^2}{4} \ \big|^1_0$

$= \frac{2}{3} + \frac{2}{5} + \frac{1}{4}$ 

$= \frac{40 + 24 + 15}{60}$

$v = \frac{79}{60}\ unidades^3$