### Ejercicio 24

$f(x,y,z) = (x^2 e^{-z})\textbf{i} + (y^3 \ln x)\textbf{j} + (z \cosh y)\textbf{k}$

$div\vec F = \frac{∂}{∂x}(x^2 e^{-z}) + \frac{∂}{∂y}(y^3 \ln x) + \frac{∂}{∂z}(z \cosh y)$

$div\vec F = 2xe^{-z} + 3y^2\ln x - \cosh y$
- - -
$rot\vec{F} = \nabla × \vec{F} = $
\begin{array}{ccc} 
+ & - & + \\
\textbf{i} & \textbf{j} & \textbf{k} \\
\frac{∂}{∂x} & \frac{∂}{∂y} & \frac{∂}{∂z} \\
(x^2 e^{-z}) & (y^3 \ln x) & (z \cosh y)
\end{array}

$= [\frac{∂}{∂y}(z \cosh y) - \frac{∂}{∂z}(y^3 \ln x)]\textbf{i} - [\frac{∂}{∂x}(z \cosh y) - \frac{∂}{∂z}(x^2 e^{-z})]\textbf{j} + [\frac{∂}{∂x}(y^3 \ln x) - \frac{∂}{∂y}(x^2 e^{-z})]\textbf{k} $

$= [z \sinh y - 0]\textbf{i} - [0 - x^2 (-e^{-z})]\textbf{j} + [\frac{y^3}{x} - 0]\textbf{k} $

$= [z \sinh y]\textbf{i} - [x^2e^{-z}]\textbf{j} + [\frac{y^3}{x}]\textbf{k} $