### Ejercicio 6

$f(x, y, z) = 2x + 3y$

$T: 2x + 3y + z = 6$

- - -

$2x + 3y + z = 6$
$z = 6 - 2x - 3y$

$2x + 3y = 6$
$x = 3 - \frac{3y}{2}$

- - -
$\int^3_0 \int^0_{3 - \frac{3y}{2}} \int^{6 - 2x - 3y}_0 {{{ 2x + 3y }}}\ dz\ dx\ dy$

$ = \int^3_0 \int^0_{3 - \frac{3y}{2}} {{{ 2xz + 3yz\ \big|^{6 - 2x - 3y}_0 }}}\ dx\ dy$

$ = \int^3_0 \int^0_{3 - \frac{3y}{2}} {{{ 2x(6 - 2x - 3y) + 3y(6 - 2x - 3y) }}}\ dx\ dy$

$ = \int^3_0 \int^0_{3 - \frac{3y}{2}} {{{ 12x - 4x^2 - 6xy + 18y - 6xy - 9y^2 }}}\ dx\ dy$

$ = \int^3_0 \int^0_{3 - \frac{3y}{2}} {{{ 12x - 4x^2 - 12xy + 18y - 9y^2 }}}\ dx\ dy$

$ = \int^3_0 \big(\ {{{ 6x^2 - \frac{4}{3}x^3 - 6x^2y + 18xy - 9xy^2\ \big |^0_{3 - \frac{3y}{2}} }}} \ \big)\ dy$

$ = - \int^3_0 {{{ 6(3 - \frac{3y}{2})^2 - \frac{4}{3}(3 - \frac{3y}{2})^3 - 6y(3 - \frac{3y}{2})^2 + 18y(3 - \frac{3y}{2}) - 9y^2(3 - \frac{3y}{2}) }}} \ dy$

$ = - \int^3_0 {{{ 6(\frac{9}{4}y^2 - 9y + 9) - \frac{4}{3}(\frac{27}{8}y^3 + \frac{81}{4}y^2 - \frac{81}{4}y + 27) - 6y(\frac{9}{4}y^2 - 9y + 9) + 18y(3 - \frac{3y}{2}) - 9y^2(3 - \frac{3y}{2}) }}} \ dy$

$ = -\int^3_0 \frac{27}{2}^2 - 54y + 54 - \frac{27}{10}y^3 - \frac{81}{5}y^2 + \frac{81}{5}y - \frac{108}{5} - \frac{54}{4}y^3 + 54y^2 - 54y + 54y - 27y^2 - 27y^2 + \frac{27}{2}y^3\ dy$

$ = - \int^3_0 - \frac{27}{10}y^3 - \frac{27}{10}y^2 - \frac{189}{5}y - \frac{378}{5}\ dy$

$ = -\big( {{{  - \frac{27}{40}y^4 - \frac{27}{30}y^3 - \frac{189}{10}y^2 - \frac{378}{5}y  }}} \ \big|^3_0 \ \big)$

$ = \frac{27}{40}(81) + \frac{27}{30}(27) + \frac{189}{10}(9) + \frac{378}{5}(3)$

$ = \frac{3807}{8} = 475.875$

**Respuesta:**
El volumen es de $475.875\ unidades^3$