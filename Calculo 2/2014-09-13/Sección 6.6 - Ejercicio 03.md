

## Sección 6.6

#### Ejercicio 3

$T$ es un bloque rectangular

$-1 ≤ x ≤ 3$
$0 ≤ y ≤ 2$
$-2 ≤ z ≤ 6$

$\int^3_{-1} \int^2_0 \int^6_{-2} xyz\ dz\ dy\ dx$

$= \int^3_{-1} \int^2_0 \big( \frac{xy6^2}{2} - \frac{xy(-2)^2}{2} \big) dy\ dx$

$= \int^3_{-1} \int^2_0 18xy - 2xy\ dy\ dx$

$= \int^3_{-1} \int^2_0 16xy\ dy\ dx$

$= \int^3_{-1} \big(\ 8xy^2\ \big|^2_0\ \big)\ dx$

$= \int^3_{-1} 32x\ dx$

$= 16x^2\ \big|^3_{-1}$

$16(3)^2 - 16(-1)^2$

$= 16(9) - 16$

$= 16(8)$

$= 128$

**Respuesta:**
El volumen es: $128\ unidades^3$