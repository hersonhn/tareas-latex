## Sección 7.1

### Ejercicio 22

$f(x, y, z) = (e^{xy} \sin y)\ \textbf{j} + (e^{xy} \cos z)\ \textbf{k}$

- - -

$div \vec{F} = \frac{∂}{∂y} (e^{xy} \sin y) + \frac{∂}{∂z}(e^{xy} \cos z)$

$div \vec{F} = e^{xz} \cos y - e^{xy} \sin z$

- - -
$rot\vec{F} = \nabla × \vec{F} = $
\begin{array}{ccc} 
+ & - & + \\
\textbf{i} & \textbf{j} & \textbf{k} \\
\frac{∂}{∂x} & \frac{∂}{∂y} & \frac{∂}{∂z} \\
(0) & (e^{xy} \sin y) & (e^{xy} \cos z)
\end{array}

$rot\vec{F} = [\frac{∂}{∂y} e^{xy} \cos z - \frac{∂}{∂z} e^{xz} \sin y]\textbf{i} - [\frac{∂}{∂x} e^{xy} \cos z - \frac{∂}{∂z} 0]\textbf{j} + [\frac{∂}{∂x} e^{xz} \sin y - \frac{∂}{∂y} 0]\textbf{k}$

$rot\vec{F} = (x e^{xy} \cos z - xe^{xz} \sin y)\textbf{i} - (ye^{xy} \cos z)\textbf{j} + (ze^{xz} \sin y)\textbf{k}$