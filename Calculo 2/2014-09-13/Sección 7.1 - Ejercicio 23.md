### Ejercicio 23

$f(x, y, z) = (x + \sin yz)\textbf{i} + (y + \sin xz)\textbf{j} + (z + \sin xy)\textbf{k}$

$div\vec F = \frac{∂}{∂x}(x + \sin yz) + \frac{∂}{∂y}(y + \sin xz) + \frac{∂}{∂z}(z + \sin xy)$

$div\vec F = 1 + 1 + 1$

$div\vec F = 3$

- - -
$rot\vec{F} = \nabla × \vec{F} = $
\begin{array}{ccc} 
+ & - & + \\
\textbf{i} & \textbf{j} & \textbf{k} \\
\frac{∂}{∂x} & \frac{∂}{∂y} & \frac{∂}{∂z} \\
(x + \sin yz) & (y + \sin xz) & (z + \sin xy)
\end{array}

$rot\vec F = [\frac{∂}{∂y}(z + \sin xy) - \frac{∂}{∂z}(y + \sin xz)]\textbf{i} - [\frac{∂}{∂x}(z + \sin xy) - \frac{∂}{∂z}(x + \sin yz)]\textbf{j} - [\frac{∂}{∂x}(y + \sin xz) - \frac{∂}{∂y}(x + \sin yz)]\textbf{k}$

$rot\vec F = [x \cos (xy) - x \cos (xz)]\textbf{i} - [y \cos (xy) - y \cos (yz)]\textbf{j} + [z \cos(xz) - z \cos(yz)]\textbf{k}$