### Ejercicio 15

$y = x^2$
$x = y^2$
$z = 0$
$z = 10 - x^2 - y^2$

- - - 

$\int^1_0 \int^{\sqrt x}_{x^2} \int^{10 - x^2 - y^2}_0 dz \ dy \ dx$

$= \int^1_0 \int^{\sqrt x}_{x^2} \big( {{{ z \ \big|^{10 - x^2 - y^2}_0 }}} \big) dy \ dx$

$= \int^1_0 \int^{\sqrt x}_{x^2} \big( {{{ 10 - x^2 - y^2 }}} \big) dy \ dx$

$= \int^1_0 \big( {{{ 10y - x^2y - \frac{y^3}{3} }}} \big|^{\sqrt x}_{x^2}  \ \big) \ dx$

$= \int^1_0 \big( {{{ 10{\sqrt x} - x^2{\sqrt x} - \frac{({\sqrt x})^3}{3} }}} \big) - \big( {{{ 10x^2 - x^4 - \frac{x^6}{3} }}} \big) \ dx$


$= \int^1_0 \big( {{{  10x^{\frac{1}{x}} -x^{\frac{5}{2}} - \frac{x^{\frac{3}{2}}}{3} - 10x^2 + x^4 + \frac{x^6}{3} }}} \big) \ dx$

$= \frac{10}{\frac{2}{3}}x^{\frac{3}{2}} - \frac{2}{7}x^{\frac{7}{2}} - \frac{2}{15}x^{\frac{5}{2}} - \frac{10}{3}x^3 + \frac{x^2}{5} + \frac{x^3}{21} \ \big|^1_0$

$= 15 - \frac{2}{7} - \frac{2}{15} - \frac{10}{3} + \frac{1}{5} + \frac{1}{21}$

$= \frac{1207}{105} = 11.49$

**Respuesta:**
El volumen es de $11.49\ unidades^3$