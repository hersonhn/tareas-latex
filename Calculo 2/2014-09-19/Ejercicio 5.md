### Ejercicio 5A

Comprobar el Teorema de Green para las siguientes integrales sobre una trayectoria cerrada.

$\oint_c x^2 dx + xy^2 dy$

Donde  $C$ es la trayectoria de $(0, 0) \to (1, 0) \to (0, 1) \to (0, 0)$

#### Integrales de línea

a. $(0, 0) \to (1, 0)$, $y = 1$, $dy = 0\ dx$ 
b. $(1, 0) \to (0, 1)$, $x = 0$, $dx = 1\ dy$
c. $(0, 1) \to (0, 0)$, $x = 1$, $dx = 0\ dy$

#### Sección A de la línea:

$\int_a x^2 dx + xy^2 dy$

$\int_1^0 x^2\ dx + 0\ dx$

$= \frac{1}{3}x^3 \ dx$

$= 0 - \frac{1}{3}$

$= -\frac{1}{3}$


#### Sección B de la línea:

$\int_a x^2 dx + xy^2 dy$

$\int_0^1 (0)\ 0\ dy + (0)y^2 \ dy$

$= 0$

#### Sección C de la línea:

$\int_a x^2 dx + xy^2 dy$

$\int_0^1 -(-y + 1)^2 - y^3 + y^2 \ dy$

$= \frac{5}{12}$

#### Valor total de la integral de línea

$= \frac{5}{12} - \frac{1}{3}$

$= \frac{5}{12} - \frac{4}{12}$

$= \frac{1}{12}$

- - -

#### Integral Doble

$\int^1_0\int^{1 - y}_0 (xy^2)\frac{∂}{∂x} - (x^2)\frac{∂}{∂y}$

$= \int^1_0\int^{1 - y}_0 y^2 - 0 \ dx \ dy$

$= \frac{1}{12}$

### Ejercicio 5B

Comprobar el Teorema de Green para las siguientes integrales sobre una trayectoria cerrada.

$\oint_c (x - y)\ dx + (x - y^3)\ dy$

Donde  $C$ es la trayectoria de $(0, 0) \to (1, 0) \to (1, 1) \to (0, 1) \to (0, 0)$

#### Integrales de línea

a. $(0, 0) \to (1, 0)$, $y = 0$, $dy = 0\ dx$ 
b. $(1, 0) \to (1, 1)$, $x = 1$, $dx = 0\ dy$
c. $(1, 1) \to (0, 1)$, $y = 1$, $dy = 0\ dx$
d. $(0, 1) \to (0, 0)$, $x = 0$, $dx = 0\ dy$


a) $\int_0^1 (x-0)\ dx + (x-0^3)\ 0 \ dx = \int_0^1 x\ dx = \frac{1}{2}x^2 \big|^1_0 = \frac{1}{2}$

b) $\int_0^1 (1 - y)\ 0\ dy + (1 - y^3)\ dy = \int_0^1 1 - y^3\ dx = x - \frac{1}{4}y^4 \big|^1_0  = \frac{3}{4}$

c) $\int^0_1 (x-1)\ dx + (x - 1^3)\ 0 \ dy = \int^0_1 (x-1)\ dy = \frac{1}{2}x^2 - x \big|^0_1 = \frac{1}{2}$

d) $\int^0_1 (0 - y) \ 0\ dy + (0 - y^3)\ dy = \int^0_1 -y^3\ dy = \frac{1}{4}y^4 \big|^1_0 = \frac{1}{4}$

$\frac{1}{2} + \frac{3}{4} + \frac{1}{2} + \frac{1}{4} = 2$

- - - 
#### Integral Doble

$\int^1_0 \int^1_0 (x - y^3)\frac{∂}{∂x} - (x - y^3)\frac{∂}{∂y} \ dx \ dy$

$= \int^1_0 \int^1_0 (1 - 0) - (-3y^2) \ dx \ dy$

$= \int^1_0 \int^1_0 1 + 3y^2 \ dx \ dy$

$= \int^1_0 \big( x+ 3xy^2 \big|^1_0 \big) \ dy$

$= \int^1_0 1 + 3y^2 \ dy$

$= y + y^3 \big|^1_0$

$= 1 + 1^3$

# $= 2$