### Ejecicio 8

a) $\vec F(x, y) = (1 + ye^x)\ \textbf{i} + (2y - xe^y)\ \textbf{j}$

b) $\vec F(x, y) = (2xe^y)\ \textbf{i} - (x^2 e^y)\ \textbf{j}$

### Inciso A:

$\vec F(x, y) = (1 + ye^x)\ \textbf{i} + (2y - xe^y)\ \textbf{j}$

$div \vec F = (1 + ye^x)\frac{∂}{∂x} + (2y - xe^y)\frac{∂}{∂y}$

$div \vec F = 0 + 2 - xe^y$

$div \vec F = 2 - xe^y$

- - -

$rot\vec F = (2y - xe^y) \frac{∂}{∂x} - (1 + ye^x) \frac{∂}{∂y}$

$rot\vec F = -e^y - e^x$

### Inciso B:

$\vec F(x, y) = (2xe^y)\ \textbf{i} - (-x^2 e^y)\ \textbf{j}$

$div\vec F = (2xe^y)\frac{∂}{∂x} + (x^2 e^y)\frac{∂}{∂y}$

$div\vec F = 2e^y + x^2e^y$

- - -

$rot\vec F = (-x^2 e^y)\frac{∂}{∂x} - (2xe^y)\frac{∂}{∂y}$

$rot\vec F = -2xe^y -2xe^y$

$rot\vec F = -4ye^y$
