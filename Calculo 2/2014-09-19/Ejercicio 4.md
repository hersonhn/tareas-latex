### Ejercicio 4

Evalúe la integral de línea.

$\int_c (y^2\ dx + x^2\ dy$

$C_1: y = x^2$
$C_2: y = x$

#### Para C1

> $y = x^2$
> $dy = 2x\ dx$

$\int_c y^2\ dx + x^2\ dy$

$\int^1_{-1} y^4\ dx + x^2(2x)\ dx$

$= \int^1_{-1} x^4 + 2x^3\ dx$

$= \frac{1}{5}x^5 + \frac{1}{2}x^4 \ \big|^1_{-1}$

$= (\frac{1}{5} + \frac{1}{2}) - (-\frac{1}{5} + \frac{1}{2})$

$= \frac{2}{5}$

- - - 

### Para C2

> $y = x$
> $dy = dx$

$\int_c y^2\ dx + x^2\ dy$

$\int^1_{-1} x^2\ dx + x^2\ dx$

$= \int^1_{-1} 2x^2\ dx$

$= \frac{2}{3}x^3 \ \big|^1_{-1}$

$= \frac{2}{3}(1) - \frac{2}{3}(-1)$

$= \frac{4}{3}$