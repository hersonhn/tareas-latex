### Ejercicio 6

Determinar si los siguientes campos son conservativos, de serlo determine la función de potencia:

a) $\vec F(x, y) = (1 + ye^{xy})\ \textbf{i} + (2y + xe^{xy})\ \textbf{j}$

b) $\vec F(x, y) = (3x^2y^3 + y^4)\ \textbf{i} + (3x^3y^2 + y^4 + 4xy^3)\ \textbf{j}$

- - -

#### Inciso A:

$\vec F(x, y) = (1 + ye^{xy})\ \textbf{i} + (2y + xe^{xy})\ \textbf{j}$

$\frac{∂}{∂y}P = \frac{∂}{∂x}Q$

$\frac{∂}{∂y}(1 + ye^{xy}) = \frac{∂}{∂x}(2y + xe^{xy})$

$y\frac{∂}{∂y}[e^{xy}] + e^{xy}\frac{∂}{∂y}[y] = x\frac{∂}{∂x}[e^{xy}] + e^{xy}\frac{∂}{∂x}[x]$

$xye^{xy} + e^{xy} = xye^{xy} + e^{xy}$

> Es conservativo.

$v = \int P\ dx + h(y)$

$v = \int (1 + ye^{xy})\ dx$

$= x + e^{xy} + h(y)$

$h'(y) \to$

> $\int (2y + xe^{xy})\ dy$
> $y^2 + e^{xy}$

> $h'(y) = y^2$

$v = x + e^{xy} + y^2$

- - -

#### Inciso B:

$\vec F(x, y) = (3x^2y^3 + y^4)\ \textbf{i} + (3x^3y^2 + y^4 + 4xy^3)\ \textbf{j}$


$\frac{∂}{∂y}P = \frac{∂}{∂x}Q$

$\frac{∂}{∂y}(3x^2y^3 + y^4) = \frac{∂}{∂x}(3x^3y^2 + y^4 + 4xy^3)$

$9x^2y^2 + 4y^3 = 9x^2y^2 + 4y^3$

> Es conservativo.

$v = \int 3x^2y^3 + y^4\ dx$

$= x^3y^3 + xy^4 + h(y)$

$h'(y) \to$

> $\int 3x^3y^2 + y^4 + 4xy^3\ dy$

> $x^3y^3 + \frac{1}{5}y^5 + xy^4$

$v = x^3y^3 + xy^4 + \frac{1}{5}y^5$
