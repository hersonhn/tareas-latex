### Ejercicio 7A

Demostrar que la integral es independiente de la trayectoria y calcular el valor de la integral usando la función de potencia

$\int^{(1,-1)}_{(0,0)} (e^y + ye^x) \ dx + (e^x + xe^y)\ dy$

> $P = e^y + ye^x$
> $Q = e^x + xe^y$

$\frac{∂P}{∂y} = \frac{∂Q}{∂x}$

$(e^y + ye^x)\frac{∂}{∂y} = (e^x + xe^y)\frac{∂}{∂x}$

$e^y + e^x = e^x + e^y$

> Es conservativo.

$\int (e^y + ye^x) \ dx$

$= xe^y + ye^x + h(y)$

> $\int (e^x + xe^y) \ dy$
> $ye^x + xe^y + h(x)$

$v = xe^y + ye^x \ \big|^{(1,-1)}_{(0,0)}$

$v = e^{-1} - e$


### Ejercicio 7B

Demostrar que la integral es independiente de la trayectoria y calcular el valor de la integral usando la función de potencia.

$\int^{(1, -1)}_{0, 0} (2xe^y) \ dx + (x^2 e^y) \ dy$

> $P = 2xe^y$
> $Q = x^2e^y$

$\frac{∂P}{∂y} = \frac{∂Q}{∂x}$

$(2xe^y)\frac{∂}{∂y} = (x^2e^y)\frac{∂}{∂x}$

> Es conservativo.

$\int 2xe^y \ dx$

$= x^2 e^y + h(y)$

> $\int x^2 e^y \ dy$
> $x^2 e^y + h(x)$

v = $x^2 e^y \big|^{(1, -1)}_{0, 0}$

$v = (1)^2 e^{-1} - (0)^2 e^0$

$v = e^{-1}$