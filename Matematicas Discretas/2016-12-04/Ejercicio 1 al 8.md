
## Ejercicio 1

$mcd(90, 60)$

**90 y 60**

$c = \lfloor\frac{90}{60}\rfloor = 1$
$r = 90 \mod 60 = 30$

**60 y 30**

$c = \lfloor\frac{60}{30}\rfloor = 2$
$r = 60 \mod 30 = 0$

**Respuesta:** mcd = 30

## Ejercicio 2

$mcd(273, 110)$

**273 y 110**

$c = \lfloor\frac{273}{110}\rfloor = 2$
$r = 273 \mod 110 = 53$

**110 y 53**

$c = \lfloor\frac{110}{53}\rfloor = 2$
$r = 110 \mod 53 = 4$

**53 y 4**

$c = \lfloor\frac{53}{4}\rfloor = 13$
$r = 53 \mod 4 = 1$

**Respuesta:** mcd = 1

## Ejercicio 3

$mcd(1400, 220)$

**1400 y 220**

$c = \lfloor\frac{1400}{220}\rfloor = 6$
$r = 1400 \mod 220 = 80$

**220 y 80**

$c = \lfloor\frac{220}{80}\rfloor = 2$
$r = 220 \mod 80 = 60$

**80 y 60**

$c = \lfloor\frac{80}{60}\rfloor = 1$
$r = 80 \mod 60 = 20$

**60 y 20**

$c = \lfloor\frac{60}{20}\rfloor = 3$
$r = 60 \mod 20 = 0$

**Respuesta:** mcd = 20

## Ejercicio 4

$mcd(825, 315)$

**825 y 315**

$c = \lfloor\frac{825}{315}\rfloor = 2$
$r = 825 \mod 315 = 195$

**315 y 195**

$c = \lfloor\frac{315}{195}\rfloor = 1$
$r = 315 \mod 195 = 120$

**195 y 120**

$c = \lfloor\frac{195}{120}\rfloor = 1$
$r = 195 \mod 120 = 75$

**120 y 75**

$c = \lfloor\frac{120}{75}\rfloor = 1$
$r = 120 \mod 75 = 45$

**75 y 45**

$c = \lfloor\frac{75}{45}\rfloor = 1$
$r = 75 \mod 45 = 30$

**45 y 30**

$c = \lfloor\frac{45}{30}\rfloor = 1$
$r = 45 \mod 30 = 15$

**30 y 15**

$c = \lfloor\frac{30}{15}\rfloor = 2$
$r = 30 \mod 15 = 0$

**Respuesta:** mcd = 15

## Ejercicio 5

$mcd(40, 20)$

**40 y 20**

$c = \lfloor\frac{40}{20}\rfloor = 2$
$r = 40 \mod 20 = 0$

**Respuesta:** mcd = 20

## Ejercicio 6

$mcd(993, 331)$

**993 y 331**

$c = \lfloor\frac{993}{331}\rfloor = 3$
$r = 993 \mod 331 = 0$

**Respuesta:** mcd = 331

## Ejercicio 7

$mcd(4807, 2091)$

**4807 y 2091**

$c = \lfloor\frac{4807}{2091}\rfloor = 2$
$r = 4807 \mod 2091 = 625$

**2091 y 625**

$c = \lfloor\frac{2091}{625}\rfloor = 3$
$r = 2091 \mod 625 = 216$

**625 y 216**

$c = \lfloor\frac{625}{216}\rfloor = 2$
$r = 625 \mod 216 = 193$

**216 y 193**

$c = \lfloor\frac{216}{193}\rfloor = 1$
$r = 216 \mod 193 = 23$

**193 y 23**

$c = \lfloor\frac{193}{23}\rfloor = 8$
$r = 193 \mod 23 = 9$

**23 y 9**

$c = \lfloor\frac{23}{9}\rfloor = 2$
$r = 23 \mod 9 = 5$

**9 y 5**

$c = \lfloor\frac{9}{5}\rfloor = 1$
$r = 9 \mod 5 = 4$

**5 y 4**

$c = \lfloor\frac{5}{4}\rfloor = 1$
$r = 5 \mod 4 = 1$

**Respuesta:** mcd = 1

## Ejercicio 8

$mcd(32670, 2475)$

**32670 y 2475**

$c = \lfloor\frac{32670}{2475}\rfloor = 13$
$r = 32670 \mod 2475 = 495$

**2475 y 495**

$c = \lfloor\frac{2475}{495}\rfloor = 5$
$r = 2475 \mod 495 = 0$

**Respuesta:** mcd = 495
