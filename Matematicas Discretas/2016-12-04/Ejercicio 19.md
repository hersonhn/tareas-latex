
## Ejercicio 19

Codifique la palabra usando el código Huffman dado.

![](huffman.png)

**Codificar:** NEED

![](huffman-n.png) N = 010
![](huffman-e.png) NE = 010 00
![](huffman-e.png) NEE = 010 00 00
![](huffman-d.png) NEED = 010 00 00 01111

**Respuesta:** 010000001111

<style>img { vertical-align: middle; }</style>
