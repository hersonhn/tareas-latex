
## Ejercicio 21

Codifique la palabra usando el código Huffman dado.

![](huffman.png)

**Codificar:** PENNED

![](huffman-p.png) P = 0110
![](huffman-e.png) PE = 0110 00
![](huffman-n.png) PEN = 0110 00 010
![](huffman-n.png) PENN = 0110 00 010 010
![](huffman-e.png) PENNE = 0110 00 010 010 00
![](huffman-d.png) PENNED = 0110 00 010 010 00 01111

**Respuesta:** 0110000100100001111

<style>img { vertical-align: middle; }</style>

