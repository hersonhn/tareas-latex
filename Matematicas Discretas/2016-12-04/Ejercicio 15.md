
## Ejercicio 15

Decodifique la cadena de bits que usa el código Huffman dado.

![](huffman.png)

**Decodificar:** 01110100110

![](huffman-l.png) **01110**100110 = L?
![](huffman-a.png) 01110**10**0110 = LA?
![](huffman-p.png) 0111010**0110** = LAP

**Respuesta:** LAP

<style>img { vertical-align: middle; }</style>
