
## Ejercicio 17

Decodifique la cadena de bits que usa el código Huffman dado.

![](huffman.png)

**Decodificar:** 1110011101001111

![](huffman-s.png) **11**10011101001111 = S?
![](huffman-a.png) 11**10**011101001111 = SA?
![](huffman-l.png) 1110**01110**1001111 = SAL?
![](huffman-a.png) 111001110**10**01111 = SAL?
![](huffman-d.png) 11100111010**01111** = SALAD

**Respuesta:** SALAD

<style>img { vertical-align: middle; }</style>
