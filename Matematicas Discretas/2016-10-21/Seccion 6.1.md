### Sección 6.1

En los ejercicios 28 al 33, un comité de seis personas compuesto por Alicia, Benjamín, Consuelo, Adolfo, Eduardo y Francisco debe elegir un presidente, secretario y tesorero.

28. **¿Cuántas selecciones excluyen a Consuelo?**

	$^5P_3 = 40$

29. **¿Cuántas selecciones existen en las que ni Benjamín ni Francisco tienen un puesto?**

	$^4P_3 = 24$
	
30. **¿Cuántas selecciones existen en las que tanto Benjamín como Francisco tienen un puesto?**

	*Combinaciones en las que Benjamin y Francisco tiene un puesto (sin incluir a los demás)*
	$^3P_2 = 6$ 
	
	*Cobinaciones incluyendo a los demás integrantes (4)*
	$4(^3P_2) = 24$

31. **¿Cuántas selecciones hay con Adolfo en un puesto y Francisco no?**

	*Combinaciones con Adolfo en cualquier puesto:*
	$1 * 4 * 3$
	
	*Lo mimos para los 3 puestos:*
	$3(1 * 4 * 3) = 36$

32. **¿Cuántas selecciones hay que tengan a Adolfo como presidente o que no incluyan a Adolfo?**
	
	(Ignorando el error de redacción)
	*Combinaciones con Adolfo como presidente que no incluyan a X*
	$1 * 4 * 3 = 12$

33. **¿Cuántas selecciones hay donde Benjamín sea presidente o tesorero?**

	*Combinaciones con Benjamin como Presidente*
	$P_B = (1 * 5 * 4) = 20$
	 
	*Combinaciones con Benjamin como Secretario*
	$S_B = (1 * 5 * 4) = 20$
	
	*Combinaciones con Benjamin como cualquiera de los dos puestos*
	$P_B + S_B - 1 = 39$
	
- - -

En los ejercicios 34 al 41, las letras ABCDE deben usarse para formar cadenas de longitud 3.

34. **¿Cuántas cadenas se pueden formar si se permiten repeticiones?**
	
	*5 letras en 5 puestos, con repeticiones*
	$5^5 = 3125$

35. **¿Cuántas cadenas se pueden formar si no se permiten repeticiones?**
	
	*5 letras en 5 puestos, sin repeticiones*
	$5! = 120$

36. **¿Cuántas cadenas comienzan con A, cuando hay repeticiones?**
	
	*5 letras en 4 puestos (aparte del primero) con repeticiones*
	$5^4 = 625$

37. **¿Cuántas cadenas comienzan con A, si no hay repeticiones?**

	*5 letras en 4 puestos (aparte del primero) sin repeticiones*
	$4! = 24$

38. **¿Cuántas cadenas no contienen a la letra A cuando se permiten repeticiones?**

	*4 letras en 5 puestos, con repeticiones*
	$4^5 = 1024$

39. **¿Cuántas cadenas no contienen a la letra A si no hay repeticiones?**

	Ninguna, no se pueden cubrir 5 puestos con 4 letras.
	
40. **¿Cuántas cadenas contienen a la letra A, si se permiten repeticiones?**

	*5 letras en 5 puestos, con repeticiones*
	$P_{ABCDE} = 5^5 = 3125$
	
	*4 letras en 5 puestos, con repeticiones (sin incluir A)*
	$P_{BCDE} = 4^5 = 1024$
	
	*Cadenas que incluyen A, con repeticiones*
	$P_{ABCDE} - P_{BCDE} = 2101$
	
41. **¿Cuántas cadenas contienen a la letra A si no se permiten repeticiones?**

	*(Letra A en una sola posición)*
	*4 letras en 4 puestos, sin repeticiones*
	$A_1 = 4! = 24$
	
	*Letra A en una cualquier posición posición*
	$A_{12345} = 5 * 4! = 120$

- - -

Los ejercicios 42 al 52 se refieren a los enteros entre 5 y 200, inclusive.

42. **¿Cuántos números hay?**

	*Cantidad de números (incluyendo el 200 y el 5)*
	$N_{5 \to 200} = 196$
	$200 - 5 + 1 = 196$

43. **¿Cuántos son pares?**

	*Cantidad de pares*
	$\frac{196}{2} = 98$

44. **¿Cuántos son impares?**

	*Cantidad de pares*
	$\frac{196}{2} = 98$

45. **¿Cuántos son divisibles entre 5?**

	*Divisibles entre 5 (incluyéndolo)*
	$200 % 5 = 40$

46. **¿Cuántos son mayores que 72?**
	
	*Mayores que 72*
	$200 - 72 = 128$

47. **¿Cuántos consisten en dígitos diferentes?**

	*Dígitos repetidos (R)*
	*Múltiplos de 11*
	$N_{11x} = 9$
	
	*Múltiplos de 11 + 100*
	$N_{11x + 100} = 9$
	
	*Múltiplos de 100*
	$N_{100x} = 2$
	
	*Números del 100 al 199 que terminan en 1 (sin contar el 111)*
	$N_{...1} = 9$
	
	*Números del 110 al 119 (sin contar el 111)*
	$N_{11...} = 9$
	
	*Dígitos repetidos*
	$R = N_{11x} + N_{11x + 100} + N_{100x} + N_{...1} + N_{11...}$
	$R = 9 + 9 + 2 + 9 + 9$
	$R = 38$
	
	*Números con dígitos diferentes*
	$D_D = N_{5 \to 200} - R$
	$D_D = 196 - 38$
	$D_D = 158$
	
48. **¿Cuántos contienen el dígito 7?**

	*Terminan en 7 (Del 5 al 100)*
	$N_{...7} = 10$
	
	*Comienzan con 7 (Del 5 al 100 sin incluir el 77)*
	$N_{7...} = 9$
	
	*Del 5 al 100*
	$N_{5 \to 100} = N_{7...} + N_{...7}$
	$N_{5 \to 100} = 10 + 9$
	$N_{5 \to 100} = 19$
	
	*Del 101 al 200*
	$N_{101 \to 200} = N_{5 \to 100}$
	$N_{101 \to 200} = 19$
	
	*Del 5 al 200*
	$N_{5 \to 200} = N_{5 \to 100} + N_{101 \to 200}$
	$N_{5 \to 200} = 19 + 19$
	$N_{5 \to 200} = 38$

49. **¿Cuántos no contienen el dígito 0?**

    *Múltiplos de 10*
    $N_{10x} = \frac{200}{10}$
    $N_{10x} = 20$

50. **¿Cuántos son mayores que 101 y no contienen el dígito 6?**

    *Terminan en 6 (Del 101 al 200)*
    $N_{...6} = 10$
    
    *Comienzan con 16 (Del 101 al 200 sin incluir el 166)*
    $N_{16...} = 9$

    *Del 101 al 200*
    $N_{6} = N_{...6} + N_{16...}$
    $N_{6} = 10 + 9$
    $N_{6} = 19$


53. 
    **a) ¿De cuántas maneras pueden ser diferentes los meses en que cumplen años cinco personas?**
    
    $^{12}C_5 = 792$
    
	**b) ¿Cuántas posibilidades hay para los meses de los cumpleaños de cinco personas?**
	
	$^{12}P_5 = 95,040$
	
	**c) ¿De cuántas maneras pueden por lo menos dos personas entre cinco tener su cumpleaños en el mismo mes?**
	
	$^{5}P_2 = 20$

- - -

Los ejercicios 54 al 58 se refieren a un conjunto de cinco libros de computación, tres de matemáticas y dos de arte, todos diferentes.

54. **¿De cuántas maneras pueden arreglarse estos libros en una repisa?**

	$L_{10} = ^{10}P_{10}$
	$L_{10} = 3,628,800$

55. **¿De cuántas maneras pueden arreglarse éstos en una repisa si los cinco libros de computación van a la izquierda y los dos de arte a la derecha?**

	$L = ^{5}P_{5} * ^{3}P_{3} * ^{2}P_{2}$
	$L = 120 * 6 * 2$
	$L = 1440$
	

56. **¿De cuántas maneras se pueden arreglar estos libros en una repisa si los cinco de computación van a la izquierda?**

	$L = ^{5}P_{5} * ^{5}P_{5}$
	$L = 120 * 120$
	$L = 14400$

57. **¿De cuántas maneras se pueden arreglar estos libros en una repisa si se agrupan todos los libros de la misma disciplina?**

	*Organización de los libros agrupados por disciplina*
	$L_1 = ^{5}P_{5} * ^{3}P_{3} * ^{2}P_{2}$
	$L_1 = 120 * 6 * 2$
	$L_1 = 1440$
	
	*Permutación de disciplinas*
	$L_2 = ^{3}P_{3}$
	$L_2 = 6$
	
	*Agrupaciones totales*
	$L = L_1 * L_2$
	$L = 1440 * 6$
	$L = 8640$

58. **¿De cuántas maneras se pueden arreglar estos libros en una repisa si los dos libros de arte no quedan juntos?**

	*Total de combinaciones posibles*
	$L_{10} = ^{10}P_{10}$
	$L_{10} = 3,628,800$
	
	*Combinaciones en las que los libros de arte quedan juntos*
	$L_{A} = 9$
	
	*Combinaciones en las que los dos de arte no quedan juntos*
	$L_{\not A} = L_{10} - L_{A}$
	$L_{\not A} = 3,628,800 - 9$
	$L_{\not A} = 3,628,791$
