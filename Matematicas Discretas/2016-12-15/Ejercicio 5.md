
## Ejercicio 5

Encuentre la forma normal disyuntiva de las siguientes expreciones booleanas

1. $f(x,y) = x + \bar{y}$

	$x(y + \bar{y}) + \bar{y}(x + \bar{x})$
	$xy + x\bar{y} + x\bar{y} + \bar{x} \bar{y}$
	$xy + x\bar{y} + \bar{x} \bar{y}$

2. $f(x,y,z) = \overline{x + \bar{z}} + z\overline{(xy + z)}$

	$\overline{x + \bar{z}} + z\overline{(xy + z)}$
	$\bar{x}z + z\overline{(xy + z)}$
	$\bar{x}z + z((\bar{x} + \bar{y}) (\bar{z}))$
	$\bar{x}z + (\bar{x} + \bar{y})$
	$\bar{x}z + \bar{x} + \bar{y}$ 
	$\bar{x} + \bar{y}$ 
	$\bar{x}(y + \bar{y}) + (x + \bar{x})\bar{y}$
	$\bar{x}y + \bar{x}\bar{y} + x\bar{y} + \bar{x}\bar{y}$
	$\bar{x}y + \bar{x}\bar{y} + x\bar{y}$

3. $f(x,y,z) = x + y + z$
	
	$x + y + z$
	$x(y + \bar{y}) + y(x + \bar{x}) + z$
	$xy + x\bar{y} + xy + \bar{x}y + z$
	$xy + x\bar{y} + \bar{x}y + z$
	$xy(z + \bar{z}) + x\bar{y}(z + \bar{z}) + \bar{x}y(z + \bar{z}) + z$
	$xyz + xy\bar{z} + x\bar{y}z + x\bar{y}\bar{z} + \bar{x}yz + \bar{x}y\bar{z} + z$
	$xyz + xy\bar{z} + x\bar{y}z + x\bar{y}\bar{z} + \bar{x}yz + \bar{x}y\bar{z} + z(x + \bar{x})$
	$xyz + xy\bar{z} + x\bar{y}z + x\bar{y}\bar{z} + \bar{x}yz + \bar{x}y\bar{z} + xz + \bar{x}z$
	$xyz + xy\bar{z} + x\bar{y}z + x\bar{y}\bar{z} + \bar{x}yz + \bar{x}y\bar{z} + x(y + \bar{y})z + \bar{x}(y + \bar{y})z$
	$xyz + xy\bar{z} + x\bar{y}z + x\bar{y}\bar{z} + \bar{x}yz + \bar{x}y\bar{z}$

3. $f(x,y) = (x + y) + (\bar{x} + \bar{y})$

	$(x + y) + (\bar{x} + \bar{y})$
	$x(y + \bar{y}) + (x + \bar{x})y + \bar{x}(y + \bar{y}) + (x + \bar{x})\bar{y}$
	$xy + x\bar{y} + xy + \bar{x}y + \bar{x}y + \bar{x}\bar{y} + x\bar{y} + \bar{x}\bar{y}$
	$xy + x\bar{y} + \bar{x}y + \bar{x}\bar{y}$

