
Utilice el mapa de Karnaugh para simplificar las funciones lógicas y dibuje cada circuito combinatorio correspondiente.

## Ejercicio 6a

|$x$|$y$|$z$|$f(x,y,z)$|
|:-:|:-:|:-:|:--------:|
| 1 | 1 | 1 | 1        |
| 1 | 1 | 0 | 0        |
| 1 | 0 | 1 | 0        |
| 1 | 0 | 0 | 1        |
| 0 | 1 | 1 | 0        |
| 0 | 1 | 0 | 0        |
| 0 | 0 | 1 | 1        |
| 0 | 0 | 0 | 1        |

![](img/6a.png)

$f(x,y,z) = A + B + C$
$f(x,y,z) = \bar{y}\bar{z} + \bar{x}\bar{y} + xyz$
