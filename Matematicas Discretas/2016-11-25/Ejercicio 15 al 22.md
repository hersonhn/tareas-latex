
Encuentre el máximo común divisor de cada par de enteros de los ejercicios 12 al 24.

## Ejercicio 15

$MCM(110, 273)$

**Factores 110:** 2, 5, 11
**Factores 273:** 3, 7, 13

$110 = 2^1 * 3^0 * 5^1 * 7^0 * 11^1 * 13^0$
$273 = 2^0 * 3^1 * 5^0 * 7^1 * 11^0 * 13^1$

$MCD = 2^0 * 3^0 * 5^0 * 7^0 * 11^0 * 13^0$
$MCD = 1$


## Ejercicio 17

$MCD(315, 825)$

**Factores 315:** 3^2, 5, 7
**Factores 825:** 3, 5^2, 11

$315 = 3^2 * 5^1 * 7^1 * 11^0$
$825 = 3^1 * 5^2 * 7^0 * 11^1$

$MCD = 3^1 * 5^1 * 7^0 * 11^0$
$MCD = 15$

## Ejercicio 18

$MCD(20, 40)$

**Factores 20:** 2<sup>2</sup>, 5
**Factores 40:** 2<sup>3</sup>, 5

$20 = 2^2 * 5^1$
$40 = 2^3 * 5^1$

$MCD = 2^2 * 5^1$
$MCD = 20$

## Ejercicio 20

$MCD(2091, 4807)$

**Factores 2091:** 3, 17, 41
**Factores 4807:** 11, 19, 23

$2091 = 3^1 * 11^0 * 17^1 * 19^0 * 23^0 * 41^1$
$4807 = 3^0 * 11^1 * 17^0 * 19^1 * 23^1 * 41^0$

$MCD = 3^0 * 11^0 * 17^0 * 19^0 * 23^0 * 41^0$
$MCD = 1$


## Ejercicio 22

$MCD(15, 15^9)$

**Factores 15:** 3, 5
**Factores 15<sup>9</sup>:** 3<sup>9</sup>, 5<sup>9</sup>

$15 = 3^1 * 5^1$
$15^9 = 3^9 * 5^9$

$MCD = 3^1 * 5^1$
$MCD = 15$
