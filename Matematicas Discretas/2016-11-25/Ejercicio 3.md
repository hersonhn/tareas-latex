
## Ejercicio 3

$n=209$

*En el ejercicios, siga el algoritmo para la entrada indicada*

> Este algoritmo determina si el entero $n > 1$ es primo. Si n es primo, el algoritmo regresa 0. Si $n$ es compuesto, el algoritmo regresa un divisor $d$ que satisface $2 ≤ d ≤ \sqrt n$. Para probar si $d$ divide a $n$, el algoritmo verifica si el residuo al dividir $n$ entre $d$, $n \mod{d}$, es cero.> ```python> from math import floor, sqrt
> > # Entrada: n 
> # Salida: d> def es_primo(n):>     for d in range(2, floor(sqrt(n))):>         if (n mod d == 0):
>             return d>     return 0
> ```

$N_{limite} = \lfloor \sqrt 209 \rfloor$
$N_{limite} = 14$

$209 \mod 2 = 1$
$209 \mod 3 = 2$
$209 \mod 4 = 1$
$209 \mod 5 = 4$
$209 \mod 6 = 5$
$209 \mod 7 = 6$
$209 \mod 8 = 1$
$209 \mod 9 = 2$
$209 \mod 10 = 9$
$209 \mod 11 = 0$

**Respuesta:** 209 no es primo.

