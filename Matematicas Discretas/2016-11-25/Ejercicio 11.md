
## Ejercicio 11

Encuentre la factorización prima de 11!

$11! = 2 * 3 * 4 * 5 * 6 * 7 * 8 * 9 * 10 * 11$
$11! = 2 * 3 * 2^2 * 5 * (2 * 3) * 7 * 2^2 * 3^2 * (2 * 5) * 11$
$11! = 2 * 3 * 2^2 * 5 * 2 * 3 * 7 * 2^2 * 3^2 * 2 * 5 * 11$
$11! = 2 * 2 * 2 * 2^2 * 2^3 * 3 * 3 * 3^2 * 5 * 5 * 7 * 11$
$11! = 2^8 * 3^4 * 5^2 * 7 * 11$

Encuentre el máximo común divisor de cada par de enteros de los ejercicios 12 al 24.

