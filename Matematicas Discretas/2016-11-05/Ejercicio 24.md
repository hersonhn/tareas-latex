## Ejercicio 24

$3^n + 7^n – 2$ es divisible entre 8, para toda $n ≥ 1$.

### Para 2:

$3^2 + 7^2 - 2 = 8x$
$9 + 49 - 2 = 8x$
$58 - 2 = 8x$
$56 = 8x$
$x = 7$

### Para 3:

$3^3 + 7^3 - 2 = 8x$
$27 + 344 - 2 = 8x$
$368 = 8x$
$x = 46$

### Para n + 1:

$3^n + 7^n – 2 = 8x$
$3^{n+1} + 7^{n+1} – 2 = 8y$
$3^{n+1} + 7^{n+1} – 2 + 3(3^n + 7^n – 2) = 8y + 3(8x)$
$(3)3^n + (7)7^n – 2 + (3)3^n + (3)7^n – 6 = 8y + 24x$
$(6)3^n + (10)7^n – 8 = 8(y + 3x)$

- $(6)3^n$ es divisible entre 8.
- $(10)7^n$ es divisible entre 8.
- $-8$ es divisible entre 8.
