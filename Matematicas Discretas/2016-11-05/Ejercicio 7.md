## Ejercicio 7

$\frac{1}{1 • 3} + \frac{1}{3 • 5} + \frac{1}{5 • 7} + ... + \frac{1}{(2n - 1)(2n + 1)} = \frac{n}{2n + 1}$

### Para 2:

$\frac{1}{1•3} + \frac{1}{3•5} = \frac{2}{4 + 1}$

$\frac{1}{3} + \frac{1}{15} = \frac{2}{5}$

$\frac{2}{5} = \frac{2}{5}$

### Para 3:

$\frac{1}{1•3} + \frac{1}{3•5} + \frac{1}{5•7} = \frac{2}{4 + 1}$

$\frac{1}{3} + \frac{1}{15} + \frac{1}{35} = \frac{3}{7}$

$\frac{3}{7} = \frac{3}{7}$

### Para n + 1:

$\frac{n}{2n + 1} + \frac{1}{(2(n + 1) - 1)(2(n + 1) + 1)} = \frac{n + 1}{2(n + 1) + 1}$

$\frac{n}{2n + 1} + \frac{1}{(2n + 2 - 1)(2n + 2 + 1)} = \frac{n + 1}{2n + 2 + 1}$

$\frac{n}{2n + 1} + \frac{1}{(2n + 1)(2n + 3)} = \frac{n + 1}{2n + 3}$

$\frac{n(2n + 3) + 1}{(2n + 1)(2n + 3)} = \frac{n + 1}{2n + 3}$

$\frac{(2n + 1)(n + 1)}{(2n + 1)(2n + 3)} = \frac{n + 1}{2n + 3}$

$\frac{n + 1}{2n + 3} = \frac{n + 1}{2n + 3}$
