## Sección 1.1

23. $(p∧q)∨(¬p∨q)$

	| P | Q | ¬P | P∧Q | ¬P∨Q | (P∧Q)∨(¬P∨Q) |
	|:-:|:-:|:--:|:---:|:----:|:------------:|
	| V | V | F  | V   | V    | V            |
	| V | F | F  | F   | F    | F            |
	| F | V | V  | F   | V    | V            |
	| F | F | V  | F   | V    | V            |

24. $¬(p∧q)∨(r∧¬p)$

	| P | Q | R | ¬Q | P∧Q | ¬(P∧Q) | R∧¬Q | ¬(P∧Q)∨(R∧¬P) |
	|:-:|:-:|:-:|:--:|:---:|:------:|:----:|:-------------:|
	| V | V | V | F  | V   | F      | F    | F             |
	| V | V | F | F  | V   | F      | F    | F             |
	| V | F | V | V  | F   | V      | V    | V             |
	| V | F | F | V  | F   | V      | F    | V             |
	| F | V | V | F  | F   | V      | F    | V             |
	| F | V | F | F  | F   | V      | F    | V             |
	| F | F | V | V  | F   | V      | V    | V             |
	| F | F | F | V  | F   | V      | F    | V             |

25. $(p∨q)∧(¬p∨q)∧(p∨¬q)∧(¬p∨¬q)$

	| P | Q | ¬P | ¬Q | P∨Q | ¬P∨Q | P∨¬Q | ¬P∨¬Q | (P∨Q)∧(¬P∨Q)∧(P∨¬Q)∧(¬P∨¬Q) |
	|:-:|:-:|:--:|:--:|:---:|:----:|:----:|:-----:|:---------------------------:|
	| V | V | F  | F  | V   | V    | V    | F     | F                           |
	| V | F | F  | V  | V   | F    | V    | V     | F                           |
	| F | V | V  | F  | V   | V    | F    | V     | F                           |
	| F | F | V  | V  | F   | V    | V    | V     | F                           |

26. $¬(p∧q)∨(¬q∨r)$

	| P | Q | R | ¬Q | P∧Q | ¬(P∧Q) | ¬Q∨R | ¬(P∧Q)∨(¬Q∨R) |
	|:-:|:-:|:-:|:--:|:---:|:------:|:----:|:-------------:|
	| V | V | V | F  | V   | F      | V    | V             |
	| V | V | F | F  | V   | F      | F    | F             |
	| V | F | V | V  | F   | V      | V    | V             |
	| V | F | F | V  | F   | V      | V    | V             |
	| F | V | V | F  | F   | V      | V    | V             |
	| F | V | F | F  | F   | V      | F    | V             |
	| F | F | V | V  | F   | V      | V    | V             |
	| F | F | F | V  | F   | V      | V    | V             |
	
	
