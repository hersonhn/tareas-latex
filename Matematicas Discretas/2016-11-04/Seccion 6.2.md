## Sección 6.2

*Los ejercicios 31 al 36 se refieren a un club cuyos miembros son 6 hombres y 7 mujeres.*31. **¿De cuántas maneras se puede elegir un comité de 5 personas?**

	$Comite = 13C_5$
	$Comite = 1287$32. **¿De cuántas maneras se puede elegir un comité de 3 hombres y 4 mujeres?**

	$C_{3H4M} = 7C_4 * 6C_3$
	$C_{3H4M} = 700$

33. **¿De cuántas maneras se puede elegir un comité de 4 personas que tenga al menos una mujer?**

	*Tomando 3 hombres de los 6 y 1 mujer de las 7*
	$C_{3H4M} = 6C_3 * 7C_1$
	$C_{1M} = 140$

34. **¿De cuántas maneras se puede seleccionar un comité de 4 personas que incluya al menos un hombre?**

	*Un hombre y tres mujeres*
	$H_1 = 6C1 * 7C3$
	$H_1 = 6 + 35$
	$H_1 = 41$
	
	*Dos hombre y dos mujeres*
	$H_2 = 6C2 * 7C2$
	$H_2 = 15 + 21$
	$H_2 = 36$
	
	*Tres hombre y una mujer*
	$H_3 = 6C3 * 7C1$
	$H_3 = 20 + 7$
	$H_3 = 27$
	
	*Cuatro hombres y cero mujeres*
	$H_4 = 6C4 * 7C0$
	$H_4 = 15 + 1$
	$H_4 = 16$
	
	*Al Menos 1 hombre*
	$H = H_1 + H_2 + H_3 + H_4$
	$H = 41 + 36 + 27 + 16$
	$H = 120$


35. **¿De cuántas maneras se puede seleccionar un comité de 4 personas que incluya personas de uno y otro sexo?**

	*Tomando en cuenta todas las posibilidades de combinaciones binarias excepto en la que todos sean hombres o todos sean mujeres.*
	$C_{1M} = 2^4 - 2$
	$C_{1M} = 14$

36. **¿De cuántas maneras se puede elegir un comité de 4 personas de manera que Marta y Rodolfo no estén juntos?**
	
	*Total de combinaciones*
	$C_{13} = 13C_4$
	$C_{13} = 715$
	
	*Combinaciones en las que Marta Y Rodolfo ESTAN juntos (N=11, R=2)*
	$C_{11} = 11C_2$
	$C_{11} = 55$
	
	*Combinaciones en las que Marta Y Rodolfo no están juntos*
	$C_{x} = C_{13} - C_{11}$
	$C_{x} = 715 - 55$
	$C_{x} = 660$
	
- - -

37. **¿De cuántas maneras se puede elegir un comité de 4 republicanos, 3 demócratas y 2 independientes entre un grupo de 10 republicanos, 12 demócratas y 4 independientes?**

	$C_o = R * D * I$
	$C_o = 10C_4 * 12C_3 * 4C_2$
	$C_o = 210 + 220 + 6$
	$C_o = 436$

38. **¿Cuántas cadenas de 8 bits contienen exactamente tres ceros?**

	$C_{bits} = 8C_5$
	$C_{bits} = 56$

39. **¿Cuántas cadenas de 8 bits contienen 3 ceros seguidos y 5 unos?**

	$C_1 = $ `00011111`
	$C_2 = $ `10001111`
	$C_3 = $ `11000111`
	$C_4 = $ `11100011`
	$C_5 = $ `11110001`
	$C_6 = $ `11111000`
	
	Respuesta: *6 Cadenas*

40. **¿Cuántas cadenas de 8 bits contienen al menos 2 ceros seguidos?**

	*Combinaciones con 2 o más ceros seguidos*
	$C_{>2} = C_2 + C_3 + C_4 + C_5 + C_6 + C_7 + C_8$
	$C_{>2} = 7 + 6 + 5 + 4 + 3 + 2 + 1$
	$C_{>2} = 28$
