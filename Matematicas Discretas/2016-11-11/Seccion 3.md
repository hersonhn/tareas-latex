## Sección 3
En los ejercicios indicados dibuje un diagrama de Venn

1. $B \cap \overline{B}$

	![](s3-e1-d1.png?1) $B \cap \overline{B}$
	
	- - -

2. $B \cup \overline{B - A}$

	![](s3-e2-d1.png) $B$
	![](s3-e2-d2.png) $B - A$
	![](s3-e2-d3.png) $\overline{B - A}$
	![](s3-e2-d4.png) $B \cup \overline{B - A}$

	- - -

3. $(C \cap A) - (\overline{(B - A)} \cap C)$


	![](s3-e3-d1.png) $C \cap A$
	![](s3-e3-d2.png) $B - A$
	![](s3-e3-d3.png) $\overline{B - A}$
	![](s3-e3-d4.png) $\overline{(B - A)} \cap C$
	![](s3-e3-d4.png) $(C \cap A) - (\overline{(B - A)} \cap C)$

	- - -

4. $(\overline{A} \cup B) \cap (\overline{C} - A)$
	
	![](s3-e4-d1.png) $\overline{A} \cup B$	![](s3-e4-d2.png) $\overline{C} - A$	![](s3-e4-d3.png) $(\overline{A} \cup B) \cap (\overline{C} - A)$


<style>
	img { vertical-align: middle }
</style>
