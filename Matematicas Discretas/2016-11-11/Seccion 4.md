## Sección 4

Se tiene un grupo de 191 estudiantes, de los cuales 10 toman francés, negocios y música; 36 toman francés y negocios; 20 están en francés y música; 18 en negocios y música; 65 en francés; 76 en negocios y 63 toman música.
a) ¿Cuántos toman francés y música pero no negocios?
b) ¿Cuantos toman negocios pero no francés ni música?
c) ¿Cuantos toman francés o negocios (o ambos)?
d) ¿Cuantos toman música o francés (o ambos) pero no negocios?
e) ¿Cuantos no toman ninguna de las tres materias?

![](s4-d1.png)

| Conjunto          | Cantidad | Ecuación        |
|-------------------|----------|-----------------|
| $U$               | 191      |                 |
| $F$               | 65       | $a + x + y + n$ |
| $N$               | 76       | $b + x + z + n$ |
| $M$               | 63       | $c + y + z + n$ |
| $F \cap N$        | 36       | $x + n$         |
| $F \cap M$        | 20       | $y + n$         |
| $N \cap M$        | 10       | $z + n$         |
| $F \cap N \cap M$ | 10       | $n$             |

$x + n = 36$
$y + n = 20$
$z + n = 10$

$a + y + y + n = 65$
$b + x + z + n = 76$
$c + y + z + n = 63$

$x + n = 36 \to x = 26$
$y + n = 20 \to y = 10$
$z + n = 10 \to z = 0$

$a + y + y + n = 65$
$a + 26 + 10 + 10 = 65$
$a = 19$

$b + x + z + n = 76$
$b + 26 + 0 + 10 = 76$
$b = 40$

$c + y + z + n = 63$
$c + 10 + 0 + 10 = 63$
$c = 43$

![](s4-d2.png)

1. ¿Cuántos toman francés y música pero no negocios? 
	
	![](s4-v1.png) 
	$y = 10$
	

2. ¿Cuantos toman negocios pero no francés ni música?

	![](s4-v2.png)
	$b = 40$

3. ¿Cuantos toman francés o negocios (o ambos)?

	![](s4-v3.png)
	$a + b + x + y + z + n$
	$19 + 40 + 26 + 10 + 0 + 10$
	$105$

4. ¿Cuantos toman música o francés (o ambos) pero no negocios?

	![](s4-v4.png)
	$a + c + y$
	$19 + 40 + 10$
	$69$

5. ¿Cuantos no toman ninguna de las tres materias?

	![](s4-v5.png)
	$U - a - b - c - x - y - z - n$
	$191 - 19 - 40 - 43 - 26 - 10 - 0 - 10$
	$43$
