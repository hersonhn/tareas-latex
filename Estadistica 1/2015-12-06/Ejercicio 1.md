## Ejercicio 1

En una ciudad se estima que la temperatura máxima en el mes de junio sigue una distribución normal, con media 23° y desviación típica 5°.

Calcular el número de días del mes en los que se espera alcanzar máximas entre 21° y 27°

$\bar{x} = 23$
$\sigma = 5$
$z = \frac{x - \bar x}{\sigma}$

#### Entre 21 y 23

$\bar{x} = 23$
$\sigma = 5$
$x = 21$
$z = \frac{21 - 23}{5}$
$z = 0.4$

$P = [0.4, 0.0]$
$P = 0.1554$

$P_{(21 \to 23)} = 0.1554$

#### Entre 23 y 27

$\bar{x} = 23$
$\sigma = 5$
$x = 27$
$z = \frac{27 - 23}{5}$
$z = 0.8$

$P = [0.8, 0.0]$
$P = 0.2881$

$P_{(23 \to 27)} = 0.2881$

#### Entre 21 y 27

$P = P_{(21 \to 23)} + P_{(23 \to 27)}$

$P = 0.1554 + 0.2881$

$P = 0.4435$

$N_{dias} = 30P$

$N_{dias} = 13.3$

### Respuesta

La cantidad de días en los que la temperatura será entre 21º y 27º es de aproximadamente 13.3.

