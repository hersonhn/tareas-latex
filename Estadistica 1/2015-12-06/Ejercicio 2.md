## Ejercicio 2

La media de los pesos de 500 estudiantes de un colegio es 70 kg y la desviación típica 3 kg. Suponiendo que los pesos se distribuyen normalmente, hallar cuántos estudiantes pesan:

1. Entre 60 kg y 75 kg
2. Más de 90 kg
3. Menos de 64 kg


$\bar{x} = 70$
$\sigma = 3$
$z = \frac{x - \bar x}{\sigma}$

#### Entre 60 kg y 75 kg

*De 60 a 70 kg*

$\bar{x} = 70$
$\sigma = 3$
$x = 60$

$z = \frac{60 - 70}{3}$
$z = -3.33333$

$[3.3, 0.033] = 0.4996$
$P = 0.4996$

$P_{(60 \to 70)} = 0.4996$

*De 70 a 75*

$\bar{x} = 70$
$\sigma = 3$
$x = 75$

$z = \frac{75 - 70}{3}$
$z = -1.6666$

$[1.0, 0.0] = 0.4515$
$P = 0.4515$

$P_{(70 \to 75)} = 0.4515$

*De 60 a 75*

$P = P_{(60 \to 70)} + P_{(70 \to 75)}$
$P = 0.4996 + 0.4515 $
$P = 0.9511$


#### Más de 90 kg

$\bar{x} = 70$
$\sigma = 3$
$x = 90$

$z = \frac{90 - 70}{3}$
$z = 6.666$

$[6.6, 0.066] = 0.5$
$P_1 = 0.5$

$P_{(70 \to 90)} = \frac{P_1}{2}$
$P_{(70 \to 90)} = 0.5$

$P = 0.5 - P_{(70 \to 90)}$
$P = 0$

#### Menos de 64 kg

$\bar{x} = 70$
$\sigma = 3$
$x = 64$

$z = \frac{64 - 70}{3}$
$z = -2$

$[2.0, 0.0] = 0.4713$
$P_1 = 0.4713$

$P_{(64 \to 70)} = \frac{P_1}{2}$
$P_{(64 \to 70)} = 0.4713$

$P = 0.5 - P_{(64 \to 70)}$
$P = 0.0287$

### Respuestas:

1. Entre 60 kg y 75 kg: $0.9511$
2. Más de 90 kg: $0$
3. Menos de 64 kg: $0.0287$