Ejercicio 1
===========

¿Qué tan grande debe seleccionares una muestra para tener un intervalo de confianza de 95% y un margen de error de 10%?

$p = 0.5$
$q = 0.5$
$z = 1.96$
$e = 0.1$

$n = \frac{z^2 pq}{E^2}$

$n = \frac{(1.96^2)(0.5)(0.5)}{(0.1^2)}$

$n = 96.04$

#### Respuesta:

La muestra debe ser de 96 elementos.