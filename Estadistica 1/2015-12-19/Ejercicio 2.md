Ejercicio 2
===========

Se dese lanzar una nueva línea de refrescos en una ciudad, el margen de error máximo aceptado es del 3% determine el tamaño de la muestra para un nivel de confianza del 95%

$p = 0.5$
$q = 0.5$
$z = 1.96$ (95% de confianza)
$e = 0.03$

$n = \frac{z^2 pq}{E^2}$

$n = \frac{(1.96^2)(0.5)(0.5)}{(0.03^2)}$

$n = 1,067.11$

#### Respuesta:

Para tener un muestra acertada a esos parámetros, debe ser de 1,067 elementos.