Ejercicio 2
===========

Para evitar que lo descubran en la aduana, un viajero ha colocado 6 tabletas de narcótico en una botella que contiene 9 píldoras de vitamina que son similares en apariencia. Si el oficial de la aduana selecciona 3 tabletas aleatoriamente para analizarlas,

3. ¿Cuál es la probabilidad de que el viajero sea arrestado por posesión de narcóticos?

2. ¿Cuál es la probabilidad de que no sea arrestado por posesión de narcóticos?

#### Probabilidad de ser arrestado:

$P(x=4)$

$N = 15$
$n = 3$
$R = 6$
$x = 1$

$P(x=1) = \frac{\left(\substack{R\\x}\right)\left(\substack{N - R\\n - x}\right)}{\left(\substack{N\\n}\right)}$

$P(x=1) = \frac{\left(\substack{6\\1}\right)\left(\substack{15 - 6\\3 - 1}\right)}{\left(\substack{15\\3}\right)}$

$P(x=1) = \frac{\left(\substack{6\\1}\right)\left(\substack{9\\2}\right)}{\left(\substack{15\\3}\right)}$

$P(x=1) = \frac{\left(\frac{6!}{1!(6 - 1)!}\right)\left(\frac{9!}{2!(9 - 2)!}\right)}{\left(\frac{15!}{3!(15 - 3)!}\right)}$

$P(x=1) = \frac{(6)(36)}{(455)}$

$P(x=1) = 0.47$

#### Probabilidad de no ser arrestado:

$P(no) = 1 - P(x=1)$

$P(no) = 1 - 0.47$

$P(no) = 0.53$

### Respuestas:

1. Probabilidad de ser arrestado: $0.47$
2. Probabilidad de no ser arrestado: $0.53$