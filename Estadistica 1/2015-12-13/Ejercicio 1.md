Ejercicio 1
===========

De un lote de 10 proyectiles, 4 se seleccionan al azar y se disparan. Si el lote contiene 3 proyectiles defectuosos que no explotarán, ¿cuál es la probabilidad de que:

1. ¿los 4 exploten?
2. ¿Al menos 2 no exploten?

#### Los Cuatro exploten

$P(x=4)$

$N = 10$
$n = 4$
$R = 7$
$x = 4$

$P(x=4) = \frac{\left(\substack{R\\x}\right)\left(\substack{N - R\\n - x}\right)}{\left(\substack{N\\n}\right)}$

$P(x=4) = \frac{\left(\substack{7\\4}\right)\left(\substack{10 - 7\\4 - 4}\right)}{\left(\substack{10\\4}\right)}$


$P(x=4) = \frac{\left(\substack{7\\4}\right)\left(\substack{3\\0}\right)}{\left(\substack{10\\4}\right)}$


$P(x=4) = \frac{\left(\frac{7!}{4!(7 - 4)!}\right)\left(\frac{3!}{0!(3 - 0)!}\right)}{\left(\frac{10!}{4!(10 - 4)!}\right)}$

$P(x=4) = \frac{(35)(1)}{(201)}$

$P(x=4) = 0.16$

#### Al menos dos no exploten

###### que uno no explote:

$N = 10$
$n = 4$
$R = 3$
$x = 1$

$P(x=1) = \frac{\left(\substack{R\\x}\right)\left(\substack{N - R\\n - x}\right)}{\left(\substack{N\\n}\right)}$

$P(x=1) = \frac{\left(\substack{3\\1}\right)\left(\substack{10 - 3\\4 - 1}\right)}{\left(\substack{10\\4}\right)}$


$P(x=1) = \frac{\left(\substack{3\\1}\right)\left(\substack{7\\3}\right)}{\left(\substack{10\\4}\right)}$

$P(x=1) = \frac{\left(\frac{3!}{1!(3 - 1)!}\right)\left(\frac{7!}{3!(7 - 3)!}\right)}{\left(\frac{10!}{4!(10 - 4)!}\right)}$

$P(x=1) = \frac{(3)(35)}{(201)}$

$P(x=1) = 0.52$

###### que dos no exploten:

$N = 10$
$n = 4$
$R = 3$
$x = 2$

$P(x=2) = \frac{\left(\substack{R\\x}\right)\left(\substack{N - R\\n - x}\right)}{\left(\substack{N\\n}\right)}$

$P(x=2) = \frac{\left(\substack{3\\2}\right)\left(\substack{10 - 3\\4 - 2}\right)}{\left(\substack{10\\4}\right)}$


$P(x=2) = \frac{\left(\substack{3\\2}\right)\left(\substack{7\\2}\right)}{\left(\substack{10\\4}\right)}$

$P(x=2) = \frac{\left(\frac{3!}{2!(3 - 2)!}\right)\left(\frac{7!}{2!(7 - 2)!}\right)}{\left(\frac{10!}{4!(10 - 4)!}\right)}$

$P(x=2) = \frac{(3)(21)}{(201)}$

$P(x=2) = 0.20$

###### que uno o que dos exploten

$P(x=1,x=2) = P(x=1) + P(x=2)$

$P(x=1,x=2) = 0.52 + 0.20$

$P(x=1,x=2) = 0.72$

### Respuestas:


1. Probabilidad de que los 4 exploten: $0.16$
2. Probabilidad de que al menos 2 no exploten: $0.72$