## Ejercicio 1

Durante 2001, el 61.3% de los hogares de estados unidos compraron café de grano y gastaron un promedio de &dollar;36.16 en café de grano durante el año, considere el desembolso anual para café de grano en los hogares que lo compran, suponiendo que este desembolso se distribuye aproximadamente como una variable aleatoria normal con una media de &dollar;36.16 y una desviación  estándar de &dollar;10.00.

1. Encuentre la probabilidad de que un hogar gaste menos de &dollar;25.00

2. Encuentre la probabilidad de que un hogar gasta más de &dollar;50.00

3. Qué proporción de los hogares gastan entre &dollar;30.00 y &dollar;40.00



$\bar{x} = 36.16$
$\sigma = 10.00$
$z = \frac{x - \bar x}{\sigma}$

#### Menos de &dollar;25.00

$\bar{x} = 36.16$
$x = 25$
$\sigma = 10.00$

$z = \frac{25 - 36.16}{10}$
$z = -1.116$
$[1.1, 0.016] = 0.36$
$P = 0.36$

#### Más de &dollar;50.00

$\bar{x} = 36.16$
$x = 50$
$\sigma = 10.00$

$z = \frac{50 - 36.16}{10}$
$z = 1.384$
$[1.3, 0.084] = 0.41$
$P = 0.41$

#### Entre &dollar;30.00 y &dollar;40.00

Menor a 30

$\bar{x} = 36.16$
$x = 30$
$\sigma = 10.00$

$z_1 = \frac{30 - 36.16}{10}$
$z_1 = -0.616$
$[0.6, 0.016] = 0.22$

Mayor a 40

$\bar{x} = 36.16$
$x = 40$
$\sigma = 10.00$

$z_1 = \frac{40 - 36.16}{10}$
$z_1 = -0.384$
$[0.3, 0.084] = 0.14$

$P = 1 - (0.22 + 0.14)$
$P = 0.64$

### Respuestas:

1. La probabilidad de que un hogar gaste menos de &dollar;25.00 es de $0.36$

2. La probabilidad de que un hogar gasta más de &dollar;50.00 es de $0.41$

3. La proporción de los hogares que gastan entre &dollar;30.00 y &dollar;40.00 es de $0.64$