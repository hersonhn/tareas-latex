## Ejercicio 2

La fuerza de rompimiento de las bolsas de plástico usadas para empacar productos se distribuye normalmente con una media de 5 libras por pulgadas cuadrada, y una desviación estándar de 1.5 libras por pulgada cuadrada ¿Qué proporción de bolsas tiene una fuerza de rompimiento de?

1. Menos de 3.17 libras por pulgadas cuadradas
2. Al menos 3.6 libras por pulgadas cuadradas
3. Entre 5 y 5.5 libras por pulgada cuadrada


$\bar{x} = 5$
$\sigma = 1.5$
$z = \frac{x - \bar x}{\sigma}$

#### Menos de 3.17

$\bar{x} = 5$
$x = 1.5$
$\sigma = 3.17$

$z = \frac{3.17 - 5}{1.5}$
$z = -1.22$
$[1.2, 0.02] = 0.3888$
$P = 0.3888$

#### Menos de 3.6

$\bar{x} = 5$
$x = 1.5$
$\sigma = 3.6$

$z = \frac{3.6 - 5}{1.5}$
$z = 0.93$
$[0.9, 0.03] = 0.3238$
$P = 0.3238$

#### Entre 5 y 5.5

Mayor que 5.5

$\bar{x} = 5$
$x = 1.5$
$\sigma = 3.17$

$z_1 = \frac{5.5 - 5}{1.5}$
$z_1 = 0.33$
$[0.3, 0.03] = 0.12$
$P_1 = 0.12$

Menor que 4.5 o Mayor que 5.5

$P_2 = 2 P_1$
$P_2 = 2 (0.12)$
$P_2 = 0.24$

Entre 4.5 y 5.5

$P_3 = 1 - P_2$
$P_3 = 0.76$

De 5 a 5.5

$P_4 = \frac{P_3}{2}$
$P_4 = 0.38$


### Respuestas:

1. Menos de 3.17 libras por pulgadas cuadradas: $0.38$
2. Al menos 3.6 libras por pulgadas cuadradas: $0.32$
3. Entre 5 y 5.5 libras por pulgada cuadrada: $0.38$