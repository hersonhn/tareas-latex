## Ejercicio 2: Distribución de Poisson En la inspección de hojalata producida por un proceso electrolítico continuo, se identifican 0.2 imperfecciones en promedio por minuto. Determine las probabilidades de identificar: 

1. Una imperfección en 3 minutos
2. Al menos dos imperfecciones en 5 minutos### Una imperfección por minuto

#### Variables:

$\lambda$ = 0.2  
$x$ = 1 en 3 minutos = $\frac{1}{3}$ por minuto. = 0.333


$P(x) = \frac{e^{-\lambda} \lambda^x}{x!}$

$P(x) = \frac{e^{-0.2} 0.2^0.333}{0.333!}$

$P(x) = 0.5361$

### Al menos dos imperfecciones en 5 minutos

$\lambda$ = 0.2  
$x$ = 2 en 5 minutos = 0.4

$P(x) = P(0) + P(0.2) + P(0.4)$
$P(x) = \frac{e^{-0.2} 0.2^0.4}{0.4!}$

$P(x) = 0.4847$