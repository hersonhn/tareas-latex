## Ejercicio 1: Distribución binomial
Si un estudiante responde al azar a un examen de 8 preguntas de verdadero o falso 

- ¿Cuál es la probabilidad de que acierte 4?
- ¿Cuál es la probabilidad de que acierte dos o menos? 


### Probabilidad de acertar 4

$P(x) = \frac{n!}{x! (n - x)!} p^x (1 - p)^{n - x}$#### Variables:

$x$ = 4 aciertos  
$n$ = 8 eventos  
$p$ = 0.5 de probabilidad


$P(4) = \frac{8!}{4! (8 - 4)!} 0.5^4 (1 - 0.5)^{8 - 4}$
$P(4) = 0.27$

### Probabilidad de acertar 2 o menos

$P(\leq 2) = P(0) + P(1) + P(2)$$P(\leq 2) = $  
&emsp; &emsp; &emsp; $\frac{8!}{0! (8 - 0)!} 0.5^0 (1 - 0.5)^{8 - 0} +$  
&emsp; &emsp; &emsp; $\frac{8!}{1! (8 - 1)!} 0.5^1 (1 - 0.5)^{8 - 1} +$  
&emsp; &emsp; &emsp; $\frac{8!}{2! (8 - 2)!} 0.5^2 (1 - 0.5)^{8 - 2} +$

$P(\leq 2) = \frac{1}{256} + \frac{1}{32} + \frac{7}{26}$

$P(\leq 2) = 0.304$


### Respuestas: 

- La probabilidad de acertar 4 respuestas de 8 es de 0.27.
- La probabilidad de acertar 2 o menos respuestas es de 0.304.