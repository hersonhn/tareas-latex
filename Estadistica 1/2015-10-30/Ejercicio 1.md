### Ejercicio #1

Sean A y B dos sucesos aleatorios con $p(A) = \frac{1}{2}$, $p(B) = \frac{1}{3}$ y $p(A \cap B) = \frac{1}{4}$

Determinar:
	
1. **P(A | B)**

	$p(A\ |\ B) = p(A) - p(A \cap B)$
	
	$p(A\ |\ B) = \frac{1}{2} - \frac{1}{4}$
	
	$p(A\ |\ B) = \frac{1}{4}$

2. **P(B\ |\ A)**

	$p(B\ |\ A) = p(B) - p(A \cap B)$
	
	$p(B\ |\ A) = \frac{1}{3} - \frac{1}{4}$
	
	$p(B\ |\ A) = \frac{1}{12}$


