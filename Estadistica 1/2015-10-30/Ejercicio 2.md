### Ejercicio #2

De un baraja de 48 cartas, se extrae simultáneamente dos de ellas. Calcule la probabilidad de que las dos sean copas.

	> Copas = Corazones

Implicando que hay un mismo número de cartas para cada tipo, habrían 12 cartas de copas.
	
Primer Intento:

$P_1(copas) = \frac{12}{48}$

Sí el primer intento es exitoso, habría 1 carta menos de copas en la baraja

$P_2(copas) = \frac{11}{47}$

**Desarrollo:**

$P = P_1(copas) * P_2(copas)$

$P = \frac{12}{48} * \frac{11}{47}$

$P = \frac{132}{2256}$

$P = \frac{11}{188}$

**Respuesta:**

La probabilidad de sacar dos cartas de copas simultáneamente en una baraja de 48 cartas es de $\frac{11}{188}$.