### Ejercicio #3

Una clase está forma por 10 chicos y 10 chicas; la mitad de las chicas y la mutad de los chicos han elegido francés como asignatura optativa.

1. ¿Cuál es la probabilidad de que una persona elegida al azar sea chico o estudie francés?

2. ¿Y la probabilidad de que sea chica y no estudie francés?


| Materia \ Sexo | Femenino | Masculino | Total  |
|----------------|----------|-----------|--------|
| **Francés**    |        5 |         5 | **10** |
| **No francés** |        5 |         5 | **10** |
| **Total**      |   **10** |    **10** | **20** |

#### Chico o que estudie francés

$P(Chico\ o\ Francés) = P(Chico) + P(Francés) - P(Chico\ y\ Francés)$

$P(Chico\ o\ Francés) = \frac{10}{20} + \frac{10}{20} - \frac{5}{20}$

$P(Chico\ o\ Francés) = \frac{1}{2} + \frac{1}{2} - \frac{1}{4}$

$P(Chico\ o\ Francés) = \frac{3}{4}$

#### Chica y que no estudie francés

$P(Chica\ y\ No Francés) = \frac{5}{20}$

$P(Chica\ y\ No Francés) = \frac{1}{4}$

**Respuestas:**

1. La probabilidad de que sea chico o que estudie francés es de $\frac{3}{4}$.

2. La probabilidad de que sea chica y que no estudie francés es de $\frac{1}{4}$.
